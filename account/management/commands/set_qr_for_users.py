from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from account.models import UserModel
from zss.utills import generate_qr_token


class Command(BaseCommand):
    help = 'Create qr token and set user_qr field'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        _models = UserModel.objects.filter(Q(user_qr='0') | Q(user_qr__isnull=True))[:200]

        for u in _models:
            u.user_qr = generate_qr_token(u.id)
            u.save()

        print('DONE - ', len(_models))
