# Generated by Django 3.2.12 on 2023-11-15 01:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0013_auto_20231113_0216'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermodel',
            name='glb_me',
            field=models.IntegerField(default=5000, verbose_name='User LeaderBoard'),
        ),
    ]
