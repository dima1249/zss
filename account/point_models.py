from datetime import datetime, timedelta
from operator import itemgetter

from django.db import models
from django_paranoid.models import ParanoidModel

from account.models import UserModel
from account.serializers import UserInfoSerializer


class WatchHistoryModel(ParanoidModel):
    point = models.FloatField(default=0, null=True, blank=True, verbose_name="Оноо")
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="Хэрэглэгч", null=True,
                             related_name="history_user")
    video = models.ForeignKey("sales.VideoModel", on_delete=models.PROTECT, verbose_name="Үзвэр", null=True,
                              related_name="history_video")

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return self.id

    class Meta:
        db_table = 'watch_histories'
        verbose_name = 'Түүх'
        verbose_name_plural = 'Үзсэн түүхүүд'

    @staticmethod
    def save_history(user, video):
        item = WatchHistoryModel()
        item.point = video.loyalty_amount
        item.user = user
        item.video = video
        item.save()
        return item


class TransactionType(models.IntegerChoices):
    Collect = 1, 'Collect'
    Change = 2, 'Change'
    Income = 4, 'Income'
    Distribute = 5, 'Distribute'


class TransactionModel(ParanoidModel):
    # bank_account_number = models.CharField(verbose_name="Утасны дугаар", max_length=20, null=True, blank=True)
    # bank_account_name = models.CharField(verbose_name="Утасны дугаар", max_length=20, null=True, blank=True)
    # bank = models.CharField(verbose_name="Утасны дугаар", max_length=20, null=True, blank=True)
    transaction_type = models.IntegerField(verbose_name="Төрөл",
                                           default=TransactionType.Collect,
                                           choices=TransactionType.choices)
    point_data = models.JSONField(verbose_name="DATA", null=True, blank=True)
    desc = models.TextField(verbose_name="Description", null=True, blank=True)

    point = models.IntegerField(default=0, null=True, blank=True, verbose_name="Оноо")
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="Хэрэглэгч", null=True,
                             related_name="transaction_user")

    class Meta:
        # indexes
        db_table = 'point_transaction'
        verbose_name = 'Гүйлгээ'
        verbose_name_plural = 'Гүйлгээнүүд'

    @classmethod
    def collect(cls, point, user, point_data={}):
        item = cls()
        user.point = user.point + point
        user.save()
        item.point = point
        item.user = user
        item.desc = f"{point_data.get('key', 'NOKEY')} {point_data.get('message', '')}"
        item.point_data = point_data
        item.save()
        return item

    @classmethod
    def distribute(cls, point, user, point_data=None):
        item = cls()
        user.point = user.point - point
        user.save()
        item.transaction_type = TransactionType.Distribute
        item.point = -point
        item.user = user
        item.desc = f"{point_data.get('key', 'NOKEY')} {point_data.get('message', '')}"
        item.point_data = point_data
        item.save()
        return item

    @classmethod
    def get_lb(cls, filter_date, limit=11):
        point_data = {}
        query_set = cls.objects.filter(created_at__gt=filter_date,
                                       transaction_type=TransactionType.Collect).values(
            'user',
            'point')

        for p in query_set:
            print('p.user', p['user'], type(p['user']))
            if p['user'] in point_data:
                point_data[p['user']]['point'] = point_data[p['user']]['point'] + p['point']
            else:
                point_data[p['user']] = {
                    "point": p['point'],
                    "user": UserInfoSerializer(UserModel.objects.get(id=p['user'])).data
                }

        ordered = sorted([v for k, v in point_data.items()], key=itemgetter('point'), reverse=True)
        return ordered[:limit]

    @classmethod
    def get_month(cls):
        filter_date = datetime.now() - timedelta(days=30)
        return cls.get_lb(filter_date=filter_date)


    @classmethod
    def get_week(cls):
        filter_date = datetime.now() - timedelta(days=7)
        return cls.get_lb(filter_date=filter_date)


    @classmethod
    def get_global(cls):
        filter_date = datetime.now() - timedelta(days=365)
        return cls.get_lb(filter_date=filter_date, limit=3)


    @classmethod
    def get_q1(cls):
        filter_date = datetime.now() - timedelta(days=30)
        return cls.get_lb(filter_date=filter_date)


    @classmethod
    def get_q2(cls):
        filter_date = datetime.now() - timedelta(days=30)
        return cls.get_lb(filter_date=filter_date)



