# schema.py
from datetime import datetime, timedelta, time

import graphene
from django.db.models import Sum, Q
from graphene_django.types import DjangoObjectType

from account.models import UserModel
from account.point_models import TransactionModel, TransactionType


class UserType(DjangoObjectType):
    class Meta:
        model = UserModel
        only_fields = ('id',
                       'email',
                       "phone",
                       "first_name",
                       "last_name",)


class ScoreType(DjangoObjectType):
    class Meta:
        model = TransactionModel
        only_fields = ('id', 'point', "user", "created_at")


class ScoreItem(graphene.ObjectType):
    user_id = graphene.ID()
    total_point = graphene.Int()
    user = graphene.Field(UserType)

    def resolve_user(self, info):
        user_id = self.get('user_id')
        print('user_id', user_id)
        if user_id:
            user = UserModel.objects.get(id=user_id)
            print('user', user)
            return user
        return None


class Query(graphene.ObjectType):
    user = graphene.Field(UserType, id=graphene.Int())
    top_scores = graphene.List(ScoreType, limit=graphene.Int())
    leaderboard = graphene.List(ScoreItem, type=graphene.String(default_value="Q1"),
                                limit=graphene.Int(default_value=10))

    def resolve_user(self, info, id):
        return UserModel.objects.get(pk=id)

    def resolve_top_scores(self, info, limit):
        print('resolve_top_scores, info:', info)
        return TransactionModel.objects.order_by('-point')[:limit]

    def resolve_leaderboard(self, info, type, limit):
        # print('resolve_leaderboard, info:', info)
        query = Q()
        today_dt = datetime.now().date()
        if type == "H1":
            start_time = time(6, 0)  # 9 AM
            end_time = time(9, 0)  # 12 PM
            query = Q(created_at__range=(datetime.combine(today_dt, start_time), datetime.combine(today_dt, end_time)))

        elif type == "H2":
            start_time = time(9, 0)  # 9 AM
            end_time = time(12, 0)  # 12 PM
            query = Q(created_at__range=(datetime.combine(today_dt, start_time), datetime.combine(today_dt, end_time)))

        elif type == "H3":
            start_time = time(12, 0)  # 9 AM
            end_time = time(15, 0)  # 12 PM
            query = Q(created_at__range=(datetime.combine(today_dt, start_time), datetime.combine(today_dt, end_time)))

        elif type == "H4":
            start_time = time(15, 0)  # 9 AM
            end_time = time(18, 0)  # 12 PM
            query = Q(created_at__range=(datetime.combine(today_dt, start_time), datetime.combine(today_dt, end_time)))

        elif type == "M":
            query = Q(created_at__gt=today_dt - timedelta(days=30))

        elif type == "W":
            query = Q(created_at__gt=today_dt - timedelta(days=7))

        else:
            query = Q(created_at__gt=today_dt)
        # data = TransactionModel.objects.filter(created_at__gt=filter_date).values('user').annotate(
        #     total_point=Sum('point'))

        data = TransactionModel.objects.filter(transaction_type=TransactionType.Collect).filter(query).values('user_id').annotate(
            total_point=Sum('point')).order_by("-total_point")[:limit]
        # print(data)
        return data


class CreateUser(graphene.Mutation):
    class Arguments:
        username = graphene.String()
        email = graphene.String()

    user = graphene.Field(UserType)

    def mutate(self, info, phone, email):
        user = UserModel(phone=phone, email=email)
        user.save()
        return CreateUser(user=user)


class AddScore(graphene.Mutation):
    class Arguments:
        user_id = graphene.Int()
        value = graphene.Int()

    score = graphene.Field(ScoreType)

    def mutate(self, info, user_id, value):
        user = UserModel.objects.get(pk=user_id)
        score = ScoreType(point=value, user=user)
        score.save()
        return AddScore(score=score)


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    add_score = AddScore.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
