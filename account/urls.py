from django.urls import path, include
from rest_framework import routers

from account.views import *
from account.views.gok import BalanceView, TranferView
from account.views.leaderboard import LeaderBoardList
from account.views.mini_app_views import MiniAppViews

account = routers.DefaultRouter()
account.register(r'register/phone', RegisterViewWithPass, 'register_phone')
account.register(r'register/email', RegisterEmailView)
account.register(r'auth', AuthViews, 'auth')
account.register(r'login', LoginViews, 'login_phone')
account.register(r'del_account', DeleteAccountViews, 'delete_acc')
account.register(r'verification/code', VerificationCodeViews, 'verify_code_phone')
account.register(r'forgot', VerificationCodeViews, 'verify_code_phone')
account.register(r'update/bank_info', UpdateBankInfoView, 'update_bank_info')
account.register(r'upload/KYC', UpdateKYCView, 'update_kyc')

account.register(r'leaderboard', LeaderBoardList, 'leader_board')

account.register(r'mentor', SetMentorView, 'set_mentor')

urlpatterns = [
    path('', include(account.urls)),
    path('logout/', Logout.as_view()),

    path('blnc/', BalanceView.as_view({'post': 'create',})),
    path('wtdrv/', TranferView.as_view({'post': 'create',})),

    path('guest_jwt/', GuestJWT.as_view()),
    path('profile/', UserProfileView.as_view()),
    path("mini_app/", MiniAppViews.as_view()),
]
