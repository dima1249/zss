import re
import uuid

from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import update_last_login
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string
from rest_framework import serializers, status
from rest_framework.exceptions import APIException
from defender import utils as defender_utils
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from account.models import UserModel, RoleModel
from zss.custom_response_utils import CustomResponse
from zss.settings import DEBUG
from zss.utills import UtilsRedisCache, generate_qr_token


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token["user_id"] = user.id
        token["email"] = user.email
        token["phone"] = user.phone
        return token


def set_response(user):
    result = {
        "user": UserDetailSerializer(instance=user).data,
        "role": user.role.code if user.role else '',
        "token": None,
    }
    try:
        refresh = MyTokenObtainPairSerializer.get_token(user)
        update_last_login(None, user)
        result["token"] = str(refresh.access_token)
    except:
        pass
    return result


class CustomException(APIException):
    status_code = status.HTTP_208_ALREADY_REPORTED
    default_detail = "Error"
    default_code = "208"


class UserDetailSerializer(serializers.ModelSerializer):
    picture = serializers.ImageField(source="profile_picture")
    # role = serializers.CharField(source="role.code")

    class Meta:
        model = UserModel
        fields = (
            "first_name",
            "last_name",
            "birthday",
            "gender",
            "phone",
            "dial_code",
            "email",
            "username",
            "picture",
            "point",
            # "role__code",
        )
        read_only_fields = ("dial_code", "phone", "email", "username")


class UserFunctionsMixin:
    @property
    def builder(self):
        base_data = self.data.copy()
        base_data.pop("code", "")
        base_data.pop("password", "")
        if base_data.get("email"):
            base_data["email__iexact"] = base_data.pop("email", None)
        return base_data

    def get_user(self):
        users = UserModel.objects.filter(
            is_active=True, **self.builder
        )
        return users.last()

    def get_cache_key(self):
        return ("Verification_code_" + "_".join(self.builder.values())).lower()


class DefenderMixin:
    def is_blocked(self):
        if defender_utils.is_already_locked(
                self.context, username=self.get_cache_key()
        ):
            raise CustomException("ACCOUNT_VERIFICATION_CODE_BLOCKED")

    def login_blocked(self):
        if defender_utils.is_already_locked(self.context, username=self.user.username):
            raise CustomException("ACCOUNT_BLOCKED")

    def login_attempt(self, login_successful=True, is_verify=False):
        username = self.get_cache_key() if is_verify else self.user.username
        defender_utils.add_login_attempt_to_db(
            self.context, login_valid=login_successful, username=username
        )

    def clear_attempt(self, login_successful=True, is_verify=False):
        username = self.get_cache_key() if is_verify else self.user.username
        defender_utils.check_request(
            self.context, login_unsuccessful=not login_successful, username=username
        )


class UserBaseSerializer(serializers.Serializer, UserFunctionsMixin):
    def to_internal_value(self, data):
        data["dial_code"] = re.sub("\W+", "", str(data.get("dial_code", "976")))
        return super().to_internal_value(data)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.initial_data.get("phone"):
            self.fields["dial_code"] = serializers.CharField(max_length=4, min_length=1)
            self.fields["phone"] = serializers.RegexField(
                regex=r"^[0-9]+$", max_length=40, min_length=5
            )
            self.fields["phone"].default_error_messages[
                "invalid"
            ] = "ACCOUNT_INVALID_PHONE"
        else:
            self.fields["email"] = serializers.EmailField(max_length=50)


def send_verification_code(*args, **kwargs):
    code = '0000'
    # code = (
    #     "0000"
    #     if DEBUG
    #     else get_random_string(length=4, allowed_chars="1234567890")
    # )
    # if len(kwargs) == 2:
    #     desc = f"Tapatrip: Batalgaajuulah code: {code}"
    #     sms = SMSHandler()
    #     response = sms.send_sms(
    #         kwargs["dial_code"],
    #         kwargs["phone"],
    #         desc,
    #     )
    # else:
    #     context = dict()
    #     context["type"] = "verification"
    #     context["code"] = code
    #     context["ref_number"] = "Code_" + str(code)
    #     response = MailHandler.send_verification_email(
    #         kwargs["email"], context, args[0].language
    #     )
    #     activate(LANGUAGE_CODE)
    # return code if response else None
    return code


class SendCodeSerializer(UserBaseSerializer, DefenderMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def custom_validate(self, data):
        self.is_blocked()
        is_sended = bool(
            UtilsRedisCache.get_cache(self.get_cache_key())
        )
        if not is_sended:
            code = send_verification_code(self.context, **data)
            self.login_attempt(bool(code), is_verify=True)
            self.clear_attempt(bool(code), is_verify=True)

            if code:
                UtilsRedisCache.set_cache(
                    self.get_cache_key(), code
                )
            else:
                raise CustomException("ACCOUNT_SEND_ERROR")

        return {"status": not is_sended, "data": data}


class RegisterBaseSerializer(UserBaseSerializer, DefenderMixin):
    code = serializers.CharField(max_length=8, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def custom_validate(self, data):
        self.is_blocked()
        code = '0000'
        # code = TapaRedisCache.get_cache(self.get_cache_key(), cache_key="default")

        login_successful = data.get("code") == code
        self.login_attempt(login_successful, is_verify=True)
        self.clear_attempt(login_successful, is_verify=True)
        if not login_successful:
            raise CustomException("ACCOUNT_VERIFY_CODE_ERROR")
        return data


class PasswordSerializer(serializers.Serializer, DefenderMixin):
    password = serializers.CharField(style={"input_type": "password"})

    def validate(self, attrs):
        user = getattr(self, "user", None) or self.context.user

        assert user is not None
        try:
            validate_password(attrs["password"], user)
        except ValidationError as e:
            raise CustomException(e.messages)
        return super().validate(attrs)

    def check_password(self):
        login_successful = check_password(
            self.data.get("password", ""), self.user.password
        )
        self.login_attempt(login_successful)
        self.clear_attempt(login_successful)

        if not login_successful:
            raise CustomException('ACCOUNT_WRONG_PASSWORD')


class CustomerCreateSerializer(RegisterBaseSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def custom_validate(self, data):
        super().custom_validate(data)
        user = self.get_user()

        if user:
            return set_response(user)
        return set_response(
            UserModelSerializer(context=self.context).create(data)
        )


class DeleteUserSerializer(serializers.Serializer, UserFunctionsMixin, DefenderMixin):
    code = serializers.CharField(max_length=8, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def delete_user(self):
        if not self.user:
            raise CustomException("ACCOUNT_USER_FIND_ERROR")

        print("deleted ", self.user.id)
        self.user.is_active = False
        # self.user.__ch
        self.user.save()
        return CustomResponse(message="Account has deleted.")

    def custom_validate(self, data):
        self.is_blocked()
        code = '1234'
        # code = TapaRedisCache.get_cache(self.get_cache_key(), cache_key="default")

        login_successful = data.get("code") == code
        self.login_attempt(login_successful, is_verify=True)
        self.clear_attempt(login_successful, is_verify=True)
        if not login_successful:
            raise CustomException("ACCOUNT_VERIFY_CODE_ERROR")
        self.user = self.get_user()
        return data


class UserCreateSerializer(RegisterBaseSerializer, PasswordSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def custom_validate(self, data):
        super().custom_validate(data)
        user = self.get_user()
        if user:
            raise CustomException('ACCOUNT_REGISTERED_CUSTOMER')
        return set_response(
            UserModelSerializer(context=self.context).create(data)
        )


class UserModelSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data.pop("code", "")
        raw_password = validated_data.pop("password", get_random_string(length=8))
        validated_data["role"] = RoleModel.objects.only("id").get(code="user")
        validated_data["username"] = uuid.uuid4().hex
        user = super().create(validated_data)
        user.set_password(raw_password)
        user.user_qr = generate_qr_token(user.id)
        user.save(update_fields=["password", "user_qr"])
        return user

    class Meta:
        model = UserModel
        fields = "__all__"
        read_only_fields = ("id",)


class UserLoginSerializer(UserBaseSerializer, PasswordSerializer):
    def custom_validate(self, data):
        self.user = self.get_user()
        if not self.user:
            raise CustomException("ACCOUNT_USER_FIND_ERROR")

        self.login_blocked()
        self.check_password()

        return set_response(
            self.user
        )
