from rest_framework import permissions, status, serializers
from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet

from store.gateway.hipay import HiPay
from zss.custom_response_utils import CustomResponse
from zss.global_message import GlobalMessage as gsms


hipay = HiPay(prod=True)


class BalanceView(CreateModelMixin, GenericViewSet):
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        if user:
            rs = hipay.balance()
            return CustomResponse(rs,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)

        return CustomResponse("NO PERMISSION",
                              status=False,
                              status_code=status.HTTP_401_UNAUTHORIZED,
                              message=gsms.SAVE_ERROR)



class TTSerializer(serializers.Serializer):
    desc = serializers.CharField(required=True)
    amount = serializers.IntegerField(required=True, max_value=10000)



class TranferView(CreateModelMixin, GenericViewSet):
    serializer_class = TTSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        if user and user.hipay_id and user.customer_id:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            t_id = "7e14b65b-91be-4f9f-a5a7-742b11c62cc7"
            rs = hipay.withdraw(customer_id=user.customer_id,
                                amount=serializer.validated_data['amount'],
                                desc=serializer.validated_data['desc'],
                                t_id=t_id)
            return CustomResponse(rs,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)
        return CustomResponse("not valid user",
                              status_code=status.HTTP_208_ALREADY_REPORTED,
                              status=True,
                              message=gsms.SUCCESS)
