import requests
from django.db import models
from django.http import JsonResponse
from rest_framework import viewsets, mixins, permissions, status, generics, serializers

from account.point_models import TransactionModel
from sales.serializers import VideoListSerializer


class LBFilterType(models.IntegerChoices):
    WEEK = 7, 'Week Leader'
    MONTH = 30, 'Month Leader'
    DAY = 1, 'Day Leader'
    Q1 = 69, '6-9 hours Leader'
    Q2 = 912, '9-12 hours Leader'
    Q3 = 1215, '12-15 hours Leader'
    Q4 = 1518, '15-18 hours Leader'
    Q5 = 1821, '18-21 hours Leader'
    GLOBAL = 999, 'Global'


class LeaderBoardSerializer(serializers.Serializer):
    type = serializers.ChoiceField(choices=LBFilterType.choices, default=LBFilterType.GLOBAL)


class LeaderBoardList(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.IsAuthenticated]
    queryset = TransactionModel.objects.all()
    serializer_class = LeaderBoardSerializer

    def list(self, request, *args, **kwargs):
        serializer = LeaderBoardSerializer(data=request.GET)
        if serializer.is_valid():
            _type = serializer.validated_data['type']
            if _type == LBFilterType.MONTH:
                return JsonResponse(status=requests.codes.ok,
                                    data={"success": True, "result": TransactionModel.get_month()})
            if _type == LBFilterType.WEEK:
                return JsonResponse(status=requests.codes.ok,
                                    data={"success": True, "result": TransactionModel.get_week()})

            return JsonResponse(status=requests.codes.ok,
                                data={"success": True,
                                      "current_me": request.user.glb_me,
                                      "result": TransactionModel.get_global(),
                                      })

        return JsonResponse(status=requests.codes.already_reported, data={"success": False, "data": serializer.errors})
