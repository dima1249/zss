import _thread
import base64
import hashlib
import json
import os
import uuid
from datetime import datetime

import requests
import rsa

from account.models import UserModel, RoleModel
from account.user_serializers import CustomException, set_response
from zss.settings import DEBUG


class RSAEncrypt(object):
    """
    Instruction from GOLOMT:
        Зааварын дагуу бэлдсэн хүсэлт буюу JSON хүсэлтийг SHA256 алгоритм ашиглаж
        HASH үүсгэнэ. Үүсгэсэн текстийн урт нь 64 Byte байна.

        Тухайн үүсгэсэн 64 урттай текст өгөгдлийг өгөгдсөн түлхүүрүүдийн тусламжтай
        RSA (Mode = ECB; Padding = PKCS1Padding; Output text format = Base64) нууцлалын
        төрөл ашиглаж encrypt хийнэ. Гаралт нь http header –ийн X-Golomt-Signature утга
        болно.
    """

    def __init__(self, key: str) -> None:
        public_key_der = base64.b64decode(key)
        self.public_key = rsa.PublicKey.load_pkcs1_openssl_der(public_key_der)

    def encrypt(self, data: str) -> str:
        encrypted = rsa.encrypt(data.encode("utf-8"), self.public_key)
        return base64.b64encode(encrypted).decode()

    @staticmethod
    def get_hex(data: str):
        hash_str = hashlib.sha256(data.encode()).hexdigest()
        return hash_str


class MiniApp(object):
    def __init__(self):
        self.monpay_url = os.environ.get(
            "MONPAY_DEEPLINK_HOST", "https://wallet.monpay.mn"
        )
        self.socialpay_url = os.environ.get(
            "SOCIALPAY_MINIAPP_HOST", "https://p2p.golomtbank.com/rpc/smartapptoken/"
        )
        self.mini_app_client_id = os.environ.get(
            "MONPAY_MERCHANT_CLIENT_ID", "SwTiHLyLuTvxRUSHXH"
        )
        self.mini_app_client_secret = os.environ.get(
            "MONPAY_MERCHANT_CLIENT_SECRET", "OEcUjs1rLynSNcZRUp"
        )
        self.emongolia_consumer_secret = os.environ.get(
            "EMONGOLIA_CONSUMER_SECRET", "Y2VmMmQ2YzA3NWFmNDMxMzk3NGQ4NDk2Nzg2MzI0Njk="
        )
        self.emongolia_consumer_key = os.environ.get(
            "EMONGOLIA_CONSUMER_KEY", "cef2d6c075af4313974d849678632469"
        )
        self.emongolia_redirect_url = os.path.join(
            os.environ.get("OUR_HOST_URL", ""), "api/account/v2/mini_app/cache_token/"
        )
        self.emongolia_url = os.environ.get(
            "EMONGOLIA_URL", "https://st.e-mongolia.mn/oauth-api/oauth2"
        )
        self.consumer_token = None
        self.hipay_url = os.environ.get("HIPAY_URL", "https://test.hipay.mn") +"/v2"
        self.hipay_client_id = os.environ.get("HIPAY_CLIENT_ID", "Miniapp3")
        self.hipay_client_secret = os.environ.get(
            "HIPAY_CLIENT_SECRET", "fRtXnSTbTAznktv5CsJNER"
        )
        self.user_instance = None

        self.socialpay_host = os.environ.get(
            "SOCIALPAY_HOST", "https://sp-api.golomtbank.com/api"
        )

    def __call__(self, *args, **kwds):
        self.app = kwds.get("app")
        if self.app == "socialpay":
            self.code = self.convert_token(kwds.get("code"))
        else:
            self.code = kwds.get("code")

    def get_token_hipay(self):
        get_token_url = os.path.join(self.hipay_url, "auth/token")
        headers = {"Content-Type": "application/json"}
        token_res = requests.post(get_token_url, json=self.__repr__(), headers=headers)
        if token_res.status_code == 200 and token_res.json().get("code") == 1:
            token = token_res.json().get("access_token")
            print('token_res', token_res.json())
            print('token', token)
            get_user_info_url = os.path.join(self.hipay_url, "user/info")
            headers = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {token}",
            }
            user_info_rs = requests.get(get_user_info_url, headers=headers)
            if user_info_rs.status_code == 200 and user_info_rs.json().get("code") == 1:
                return user_info_rs.json()
        else:
            print("hipay get_token_url exception:", token_res.status_code, token_res.text, self.__repr__())
            raise CustomException(token_res.text)
        return None

    @staticmethod
    def convert_token(token):
        _token = None
        try:
            _token = token.split("=")[1]
        except IndexError:
            _token = token.replace("auth", "")
        finally:
            return _token

    def __repr__(self):
        if self.app == "socialpay":
            # data = json.dumps({"method": "checkAdditionalToken", "params": [self.code]})
            return json.dumps({"token": self.code}).replace(" ", "")
        elif self.app == "monpay":
            redirect_uri = (
                "https://staging.tapatrip.io/monpay"
                if DEBUG
                else "https://tapatrip.com/monpay"
            )
            return {
                "redirect_uri": redirect_uri,
                "client_id": self.mini_app_client_id,
                "client_secret": self.mini_app_client_secret,
                "grant_type": "authorization_code",
                "code": self.code,
            }
        elif self.app == "emongolia":
            return {
                "consumerSecret": self.emongolia_consumer_secret,
                "consumerKey": self.emongolia_consumer_key,
                "redirectURI": self.emongolia_redirect_url,
                "generateId": self.code,
            }
        elif self.app == "hi_pay":
            return {
                "client_id": self.hipay_client_id,
                "client_secret": self.hipay_client_secret,
                "redirect_uri": "",
                "grant_type": "",
                "code": self.code,
            }
        return None

    # @property
    def get_data(self):
        if self.app == "socialpay":
            # return self.get_token_socialpay()
            return self.sp_check_token()
        elif self.app == "monpay":
            return self.get_token_monpay()
        # elif self.app == "emongolia":
        #     return self.get_token_emongolia()
        elif self.app == "hi_pay":
            return self.get_token_hipay()

    # @property
    def get_token_monpay(self):
        token_url = os.path.join(self.monpay_url, "v2/oauth/token")
        data = self.__repr__()
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        response = requests.post(token_url, data=data, headers=headers)

        if response.status_code == 200:
            access_token = response.json().get("access_token")

            user_info = self.user_info(access_token)

            if user_info:
                return user_info
        return

    # @property
    def get_token_socialpay(self):
        token_url = self.socialpay_url
        data = self.__repr__()
        headers = {"Content-Type": "application/json"}

        response = requests.post(token_url, data=data, headers=headers)
        if response.status_code == 200 and "result" in response.json():
            result = response.json().get("result")
            if result.get("Result") != "FAILED":
                user_data = {
                    "username": uuid.uuid4().hex,
                    "monpay_id": None,
                    "socialpay_id": str(result.get("IndividualId")),
                    "dial_code": "976",
                    "phone": result.get("MobileNumber"),
                    "email_additional": str(result.get("Email", "")).lower(),
                    "first_name": result.get("FirstName"),
                    "last_name": result.get("LastName"),
                    "monpay_token": None,
                }

                return user_data
        return

    def user_info(self, access_token):
        get_token_url = os.path.join(self.monpay_url, "v2/api/oauth/userinfo")
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"Bearer {access_token}",
        }

        response = requests.get(get_token_url, headers=headers)

        if response.status_code == 200:
            result = response.json().get("result")

            user_data = {
                "username": uuid.uuid4().hex,
                "monpay_id": str(result.get("userId")),
                "socialpay_id": None,
                "dial_code": "976",
                "phone": result.get("userPhone"),
                "email_additional": str(result.get("userEmail", "")).lower(),
                "first_name": result.get("userFirstname"),
                "last_name": result.get("userLastname"),
                "monpay_token": access_token,
            }
            return user_data
        return

    def create_user(self, **data):
        user_model = UserModel
        user = user_model.objects.none()
        if data.get("phone"):
            user = user_model.objects.filter(
                dial_code=data.get("dial_code"), phone=data.get("phone")
            ).last()
            if user:
                data["email"] = None

        if not user and data.get("email"):
            user = user_model.objects.filter(email__iexact=data.get("email")).last()

        if not user and data.get("monpay_id"):
            user = user_model.objects.filter(monpay_id=data.get("monpay_id")).last()

        if not user and data.get("socialpay_id"):
            user = user_model.objects.filter(
                socialpay_id=data.get("socialpay_id")
            ).last()

        if not user and data.get("emongolia_id"):
            user = user_model.objects.filter(
                emongolia_id=data.get("emongolia_id")
            ).last()

        if not user and data.get("hipay_id"):
            user = user_model.objects.filter(hipay_id=data.get("hipay_id")).last()

        if user:
            self.user_instance = user
            if "customer_id" in data and not data["customer_id"]:
                data["customer_id"] = user.customer_id
            self.update_instance(**data)

        else:
            data["role"] = RoleModel.objects.get(code="user")
            user = user_model.objects.create(**data)

        if self.app == "emongolia":
            try:
                _thread.start_new_thread(self.get_passport_data, (user,))
            except Exception as e:
                print("Exception at MiniApp get_passport_data:", e)

        return set_response(user)
        # return set_response(user, monpay_token=monpay_token)

    def update_instance(self, **data):
        try:
            _update_fields = []
            for field_name in data:
                if not self.user_instance.__getattribute__(field_name) or self.user_instance.__getattribute__(field_name) != data.get(field_name):
                    _update_fields.append(field_name)
                    self.user_instance.__setattr__(field_name, data.get(field_name))
            return self.user_instance.save(update_fields=_update_fields)
        except Exception as e:
            print("Exception at MiniApp.update_instance:", e)

    def get_passport_data(self, user=None):
        # if GlobalPassangerModel.objects.filter(
        #     user=user, pax_type="emongolia"
        # ).exists():
        #     return
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.consumer_token}",
        }
        url = os.path.join(self.emongolia_url, "get-passport")
        response = requests.get(url, headers=headers)
        if response.status_code == 200 and response.json().get("data"):
            data = response.json().get("data")
            # {
            #     "birthDate": "12/10/1994",
            #     "civilId": "112207631137",
            #     "expiryDate": "08/02/2033",
            #     "firstname": "Mergengerel",
            #     "issuedDate": "08/02/2023",
            #     "issuedOrgName": "General Authority for State Registration of Mongolia",
            #     "lastname": "Dashzeveg",
            #     "nationalityCode": "MNG",
            #     "nationalityName": "MONGOLIA",
            #     "passportNum": "3334858",
            #     "passportTypeCode": "E",
            #     "regnum": "PYu94101219",
            #     "sexName": "M"
            #     "photo": "Base64 encoded text"
            # }
            try:
                # country_code = country3To2(data["nationalityCode"])
                country_code = "MNG"
                passport_data = {
                    "pax_type": "emongolia",
                    "display_name": f'{data["lastname"]} {data["firstname"]}'.upper(),
                    "first_name": str(data["firstname"]).upper(),
                    "last_name": str(data["lastname"]).upper(),
                    "nationality": f"{data['nationalityName']} ({country_code})"
                    if country_code
                    else "Mongolia (MN)",
                    "register_number": str(user.registration_number).upper()
                    if user and user.registration_number
                    else str(data["regnum"]).upper(),
                    "document_number": f'{data["passportTypeCode"]}{data["passportNum"]}'
                    if data["passportNum"] and data["passportTypeCode"]
                    else None,
                    "document_expired_date": datetime.strptime(
                        data["expiryDate"], "%d/%m/%Y"
                    ).date(),
                    "birthday": datetime.strptime(data["birthDate"], "%d/%m/%Y").date(),
                    "gender": data["sexName"],
                }
                # serializer = GlobalPassangerSerializer(data=passport_data)
                # if user and serializer.is_valid():
                #     serializer.get_or_create(user=user)
                return passport_data
            except Exception as e:
                print("Exception at Emongolia get-passport:", data, e)
        else:
            raise CustomException(
                response.text if response and response.text else "EMONGOLIA ERROR"
            )

    def get_golomt_headers(self):
        hash_str = RSAEncrypt.get_hex(self.__repr__())
        x_signature = RSAEncrypt(os.environ.get("SOCIALPAY_PUBLIC_KEY", "")).encrypt(
            hash_str
        )
        return {
            "Content-Type": "application/json",
            "X-Golomt-Cert-Id": os.environ.get("SOCIALPAY_CERT_ID", "tapatrip"),
            "X-Golomt-Signature": x_signature,
        }

    def sp_check_token(self):
        try:
            url = os.path.join(
                self.socialpay_host, "utility/miniapp/token/check?language=mn"
            )

            headers = self.get_golomt_headers()

            response = requests.post(
                url,
                data=self.__repr__(),
                headers=headers,
            )

            if response.status_code == 200 and response.json():
                result = response.json()
                user_data = {
                    "username": uuid.uuid4().hex,
                    "monpay_id": None,
                    "socialpay_id": str(result.get("individualId")),
                    "registration_number": result.get("registerNumber"),
                    "dial_code": "976",
                    "phone": result.get("mobileNumber"),
                    "email_additional": str(result.get("email", "")).lower(),
                    "first_name": result.get("firstName"),
                    "last_name": result.get("lastName"),
                    "monpay_token": None,
                }

                return user_data
            return
        except Exception as e:
            if not DEBUG:
                print("Exception at sp_check_token:", e)
            else:
                raise e
