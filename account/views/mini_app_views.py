import json
import uuid

from rest_framework import permissions, views

from account.user_serializers import CustomException
from account.views.mini_app import MiniApp
from zss.custom_response_utils import CustomResponse


class MiniAppViews(views.APIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ["get"]

    def get(self, request, *args, **kwargs):
        app = request.query_params.get("app", "monpay")
        code = request.query_params.get("code", None)
        u_data = request.query_params.get("u", None)
        if not code:
            raise CustomException("VALIDATION_ERROR")

        mini_app = MiniApp()
        mini_app(app=app, code=code)
        user_info = mini_app.get_data()
        if not user_info:
            raise CustomException("USER ERROR")

        user_data = {
            "username": uuid.uuid4().hex,
            "first_name": user_info.get("firstname"),
            "last_name": user_info.get("lastname"),
            "dial_code": "976",
            "phone": user_info.get("phone"),
            "email": str(user_info["email"]).lower()
            if user_info.get("email")
            else None,
            "hipay_id": code,
            "customer_id": user_info.get("customerId"),
            "id_number": user_info.get("regno")
        }
        print('json data', json.dumps(user_data))
        result = mini_app.create_user(**user_data)
        if u_data:
            return CustomResponse(result={**user_info, **result})
        return CustomResponse(result=result)
