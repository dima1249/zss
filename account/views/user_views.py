from django.contrib.auth import logout
from rest_framework import permissions, mixins, viewsets
from rest_framework.views import APIView

from zss.custom_response_utils import CustomResponse
from account.user_serializers import CustomerCreateSerializer, UserCreateSerializer, UserLoginSerializer, \
    SendCodeSerializer, UserBaseSerializer, DeleteUserSerializer


class Logout(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        logout(request)
        return CustomResponse({}, True)


class RegisterViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = CustomerCreateSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context=request)
        if serializer.is_valid(raise_exception=True):
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(result=_response)


class RegisterViewWithPass(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = UserCreateSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context=request)
        if serializer.is_valid(raise_exception=True):
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(result=_response)


class LoginViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = UserLoginSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context=request)

        if serializer.is_valid():
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(result=_response)

        print('serializer.errors', serializer.errors)
        return CustomResponse(result=None, status_code=208)


class DeleteAccountViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = DeleteUserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context=request)
        if serializer.is_valid():
            serializer.custom_validate(serializer.validated_data)
            _response = serializer.delete_user()
            return _response
        print(serializer.errors)
        return CustomResponse(message="NOT VALID DATA.")


class AuthViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = UserBaseSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context=request)
        if serializer.is_valid(raise_exception=True) and serializer.get_user():
            return CustomResponse(
                status=True,
                result=serializer.validated_data,
                message="ACCOUNT_REGISTERED_CUSTOMER",
                toggle_code=0,
            )
        return CustomResponse(
            status=False,
            status_code=208,
            result=serializer.validated_data,
            message="ACCOUNT_USER_NOT_FOUND",
            toggle_code=1,
        )


class VerificationCodeViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [permissions.AllowAny]
    serializer_class = SendCodeSerializer

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context=request)
        if serializer.is_valid(raise_exception=True):
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(
                status=_response.get("status"), result=_response.get("data")
            )
