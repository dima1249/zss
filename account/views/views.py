import requests
from django.utils.translation import gettext_lazy as _
from rest_framework import viewsets, mixins, permissions, status, generics
from rest_framework.mixins import CreateModelMixin
from rest_framework.serializers import Serializer
from rest_framework.viewsets import GenericViewSet

from account.serializers import *
from account.user_serializers import UserLoginSerializer, \
    SendCodeSerializer
from zss.custom_response_utils import CustomResponse
from zss.global_message import GlobalMessage as gsms


def confirm_code_email(email):
    return True


class RegisterEmailView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = UserModel.objects.all()
    serializer_class = UserEmailSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, **kwargs):

        serializer = self.get_serializer(data=request.data)
        role = RoleModel.objects.get(code='user')
        if serializer.is_valid(raise_exception=True):
            email = serializer.validated_data.get("email")
            password = serializer.validated_data.get("password")

            if UserModel.objects.filter(email=email, role=role):
                return CustomResponse(message="Бүртгэлтэй хэрэглэгч байна.", status=False,
                                      status_code=status.HTTP_208_ALREADY_REPORTED)

            if confirm_code_email(email):
                user = serializer.save(username=uuid.uuid4().hex)
                user.role = role
                user.email = email
                user.set_password(password)
                user.save()

                payload = JWT_PAYLOAD_HANDLER(user)
                jwt_token = JWT_ENCODE_HANDLER(payload)
                update_last_login(None, user)

                return CustomResponse(status=True, status_code=status.HTTP_200_OK,
                                      result={'token': jwt_token, 'user': UserProfileSerializer(instance=user).data})

            return CustomResponse(message="Баталгаажуулаагүй хэрэглэгч байна.", status=False,
                                  status_code=status.HTTP_208_ALREADY_REPORTED)


class LoginViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = UserLoginSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context=request)

        if serializer.is_valid():
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(result=_response)

        print('serializer.errors', serializer.errors)
        return CustomResponse(result=None, status_code=208)


class VerificationCodeViews(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [permissions.AllowAny]
    serializer_class = SendCodeSerializer

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context=request)
        if serializer.is_valid(raise_exception=True):
            _response = serializer.custom_validate(serializer.validated_data)
            return CustomResponse(
                status=_response.get("status"), result=_response.get("data")
            )


# Хандалт хийсэн төхөөрөмжинийн мэдээлэл дээрээс JWT үүсгэж буцаах
# Төхөөрөмжийн мэдээлэлийг бааз-д хадгалах

class GuestJWT(generics.CreateAPIView):
    serializer_class = Serializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, **kwargs):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            # pass
            request.data['device_ip'] = x_forwarded_for.split(',')[0]
        else:
            request.data['device_ip'] = request.META.get('REMOTE_ADDR')

        request.data['device_name'] = request.META.get('SERVER_NAME')
        request.data['device_os'] = request.user_agent.os.family
        request.data['mac_add'] = request.user_agent.os.family
        serializer = DeviceSaveSerializer(data=request.data)

        if serializer.is_valid():
            return CustomResponse(serializer.validated_data)
        print('error ', serializer.errors)
        return CustomResponse(result=serializer.errors,
                              status=False,
                              message=_(gsms.TOKEN_VALIDATOR_ERROR),
                              status_code=requests.codes.already_reported, )


class UserProfileView(generics.ListCreateAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserModel.objects.none()
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = UserUpdateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            print('valid', serializer.data)
            if serializer.validated_data.get("first_name"):
                user.first_name = serializer.validated_data.get("first_name")
            if serializer.validated_data.get("last_name"):
                user.last_name = serializer.validated_data.get("last_name")

            user.birthday = serializer.validated_data.get("birthday")
            user.gender = serializer.validated_data.get("gender")
            user.email = serializer.validated_data.get("email")

            # user.bank_account_number = serializer.validated_data.get("bank_account_number")
            # user.bank_account_name = serializer.validated_data.get("bank_account_name")
            # user.bank = serializer.validated_data.get("bank")
            # user.home_address = serializer.validated_data.get("home_address")
            # user.id_front = serializer.validated_data.get("id_front")
            # user.id_rear = serializer.validated_data.get("id_rear")
            # user.signature = serializer.validated_data.get("signature")
            # user.selfie = serializer.validated_data.get("selfie")
            user.save()
            return CustomResponse(UserProfileSerializer(user).data,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)

        print('invalid', serializer.errors)
        return CustomResponse(serializer.errors,
                              status=False,
                              status_code=status.HTTP_208_ALREADY_REPORTED,
                              message=gsms.SAVE_ERROR)

    def get(self, request):
        serializer = self.serializer_class(instance=request.user)
        return CustomResponse(result={"user": serializer.data}, status=True)


class UpdateBankInfoView(CreateModelMixin, GenericViewSet):
    serializer_class = UpdateBankInfoSerializer
    queryset = UserModel.objects.none()
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.serializer_class(user, data=request.data)
        if serializer.is_valid():
            serializer.update(user, serializer.validated_data)
            user.save()
            return CustomResponse(UserProfileSerializer(user).data,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)

        print('invalid', serializer.errors)
        return CustomResponse(serializer.errors,
                              status=False,
                              status_code=status.HTTP_208_ALREADY_REPORTED,
                              message=gsms.SAVE_ERROR)


class UpdateKYCView(CreateModelMixin, GenericViewSet):
    serializer_class = UpdateKYCSerializer
    queryset = UserModel.objects.none()
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            print('valid')
            setattr(user, serializer.validated_data['key'], serializer.validated_data['file'])
            user.save()
            return CustomResponse(UserProfileSerializer(user).data,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)

        print('invalid', serializer.errors)
        return CustomResponse(serializer.errors,
                              status=False,
                              status_code=status.HTTP_208_ALREADY_REPORTED,
                              message=gsms.SAVE_ERROR)


class SetMentorView(CreateModelMixin, GenericViewSet):
    serializer_class = SetMentorSerializer
    queryset = UserModel.objects.none()
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.serializer_class(user, data=request.data)
        if serializer.is_valid():
            if request.data['mentor'] == user.id:
                return CustomResponse(None,
                                      status=False,
                                      status_code=status.HTTP_208_ALREADY_REPORTED,
                                      message="Mentor id same as yours.")

            serializer.update(user, serializer.validated_data)
            user.save()
            return CustomResponse(UserProfileSerializer(user).data,
                                  status_code=status.HTTP_200_OK,
                                  status=True,
                                  message=gsms.SUCCESS)

        print('invalid', serializer.errors)
        return CustomResponse(serializer.errors,
                              status=False,
                              status_code=status.HTTP_208_ALREADY_REPORTED,
                              message=gsms.SAVE_ERROR)
