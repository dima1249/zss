from django.contrib import admin
from django_paranoid.admin import ParanoidAdmin

from game.models import WheelModel, WheelSectorModel


@admin.register(WheelSectorModel)
class WheelSectorAdmin(ParanoidAdmin):
    list_display = ["title", "bonus", "limit",  "current", "wheel"]
    list_filter = ['wheel', "bonus"]


class SectorInLine(admin.TabularInline):
    model = WheelSectorModel
    exclude = ['deleted_at', "desc"]
    extra = 1


@admin.register(WheelModel)
class WheelAdmin(ParanoidAdmin):
    list_display = ["title", "code", "point"]
    inlines = [SectorInLine]

