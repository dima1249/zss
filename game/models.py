from django.db import models
from django_paranoid.models import ParanoidModel

from zss.utills import PathAndRename


class WheelModel(ParanoidModel):
    title = models.CharField(max_length=100, unique=True, verbose_name="Нэр")
    code = models.CharField(max_length=10, verbose_name="Code", unique=True)
    link = models.CharField(max_length=245,  null=True, blank=True, verbose_name="Линк")
    desc = models.TextField(null=True, blank=True, verbose_name="Тайлбар")
    point = models.IntegerField(default=0, verbose_name="Эргүүлэх төлбөр")


    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return '%s' % self.title

    class Meta:
        db_table = 'game_wheel'
        verbose_name = 'Хүрд'
        verbose_name_plural = '1. Хүрд'


class WheelSectorModel(ParanoidModel):
    title = models.CharField(max_length=100, verbose_name="Нэр")
    link = models.CharField(max_length=245, null=True, blank=True, verbose_name="Линк")
    code = models.CharField(max_length=10, verbose_name="Code")
    img = models.ImageField(
        verbose_name="Зураг",
        upload_to=PathAndRename("wheel/img/"),
        null=True,
        blank=True,
    )
    desc = models.TextField(null=True, blank=True, verbose_name="Тайлбар")
    bonus = models.IntegerField(verbose_name="Point", default=0)
    limit = models.IntegerField(default=10, verbose_name="Хэдэн удаа")
    current = models.IntegerField(default=10, verbose_name="Одоо")

    wheel = models.ForeignKey("game.WheelModel", on_delete=models.PROTECT, verbose_name="Хүрд", related_name="sector_set")



    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return '%s' % self.title

    class Meta:
        db_table = 'game_wheel_sec'
        verbose_name = 'Нүд'
        verbose_name_plural = '2. Нүднүүд'
