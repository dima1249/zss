import random
from datetime import datetime

from rest_framework import serializers

from account.point_models import TransactionModel
from game.models import *


def wheel_picker(models_list=None):
    if not models_list or len(models_list) == 0:
        return -1

    r1 = int(random.randint(1, 1000) / 3)
    index = r1 % len(models_list)
    print('pick', index)
    if models_list[index].current + 1 >= models_list[index].limit:
        print(" ----> ", models_list[index].title, models_list[index].bonus)
        models_list[index].current = 0
        models_list[index].save()
        return models_list[index].id

    models_list[index].current = models_list[index].current + 1
    models_list[index].save()

    return wheel_picker(models_list)


class WheelListSerializer(serializers.ModelSerializer):
    class Meta:
        model = WheelModel
        fields = [
            "title",
            "code",
            "point"
        ]


class WheelSerializer(serializers.ModelSerializer):
    class Meta:
        model = WheelSectorModel
        fields = [
            "id",
            "title",
            "link",
            "code",
            "img",
            "bonus"
        ]


class WheelKeySerializer(serializers.Serializer):
    key = serializers.ChoiceField(required=True, choices=['gold', 'silver', 'bronze', ])
    # key = serializers.ChoiceField(required=True, choices=WheelModel.objects.all().values_list('code', flat=True))


class WheelPickerSerializer(serializers.Serializer):
    key = serializers.ChoiceField(required=True, choices=WheelModel.objects.all().values_list('code', flat=True))
    user = serializers.PrimaryKeyRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    def check_point(self):
        """
        GET WHEEL
        """

        wheel = WheelModel.objects.filter(code=self.validated_data['key'])
        if not wheel.exists():
            raise serializers.ValidationError("No Wheel.")

        user = None
        if hasattr(self.context.get("request", {}), "user"):
            user = self.context.get("request").user
            # print('wheel check point', user.point)

        if not user or user.point < wheel[0].point:
            raise serializers.ValidationError(
                f"Not enough score for this Wheel. You have to have more than {wheel[0].point - 1} points.")

        self.wheel = wheel[0]
        self.user = user

        return True

    def picker(self):

        sec_list = self.wheel.sector_set.all()
        picked_id = wheel_picker(
            models_list=sec_list
        )
        selected = [e for e in sec_list if e.id == picked_id]
        winner_bonus = 0
        if len(selected) > 0:
            prize = selected[0]
            if prize.code != 'retry':
                # distribute point for wheel
                TransactionModel.distribute(point=self.wheel.point,
                                            user=self.user,
                                            point_data={
                                                "key": "WHEEL",
                                                "message": "Picker distribution.",
                                                "w": self.wheel.code,
                                                "sector": WheelSerializer(sec_list, many=True).data,
                                                # "ug": user_agent.ua_string,
                                            })

            if prize.code in ['point', 'multi']:
                # user_point = self.user.point
                # user_point = self.user.todays_collect()
                # user_point = random.randint (5000, 10000)
                user_point = self.wheel.point
                winner_bonus = prize.bonus * user_point if 'multi' == prize.code else prize.bonus
                print(self.user.id, ' win ', prize.code, ' PRIZE ->', winner_bonus, f"TODAY's ({user_point})")
                TransactionModel.collect(point=winner_bonus,
                                         user=self.user,
                                         point_data={
                                             "key": "WHEEL_PRIZE",
                                             "message": "win",
                                             "sector_code": prize.code,
                                             "bonus": prize.bonus,
                                             # "ug": user_agent.ua_string,
                                         })

            if prize.code == 'super':
                print(self.user.id, ' win ', prize.code, ' PRIZE ', datetime.now())
                winner_bonus = 99999

        # TODO: save wheel pick history

        return {
            "sector": picked_id,
            "prize": winner_bonus
        }
