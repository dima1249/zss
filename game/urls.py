from django.urls import path, include
from rest_framework import routers

from game.views import WheelList, WheelDetView, WheelPickerView

router = routers.DefaultRouter()

router.register(r'wheel', WheelList, 'list_wheel')
router.register(r'wheel/detail', WheelDetView, 'get_wheel')
router.register(r'wheel/picker', WheelPickerView, 'wheel_picker')

urlpatterns = [
    path('rest/', include(router.urls)),
    # path('test/', test_view),
]
