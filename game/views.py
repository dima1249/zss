import requests
from django.http import JsonResponse
from rest_framework import mixins, viewsets, permissions
from game.serializers import *


class WheelList(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.AllowAny]
    queryset = WheelModel.objects.filter(code='bronze')
    # queryset = WheelModel.objects.all()
    serializer_class = WheelListSerializer


class WheelDetView(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.AllowAny]
    queryset = WheelSectorModel.objects.all()
    serializer_class = WheelKeySerializer

    def list(self, request, **kwargs):
        serializer = self.get_serializer(data=request.GET)
        if serializer.is_valid():
            sectors = WheelModel.objects.get(code=serializer.validated_data.get("key")).sector_set.all()
            res_serializer = WheelSerializer(sectors, many=True)
            # res_serializer.is_valid(raise_exception=True)

            return JsonResponse(data={"success": True,
                                      "data": res_serializer.data,
                                      "user_point": request.user.point if request and request.user and not request.user.is_anonymous else 0
                                      })

        return JsonResponse(status=requests.codes.no_content, data={"success": False, "error": "rq data not valid"})


class WheelPickerView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.IsAuthenticated]
    queryset = WheelModel.objects.none()
    serializer_class = WheelPickerSerializer

    def create(self, request, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.check_point()
            result = serializer.picker()
            return JsonResponse(data={"success": True, "result": result,
                                      "user_point": request.user.point if request and request.user else 0})

        return JsonResponse(status=requests.codes.no_content, data={"success": False, "error": "rq data not valid"})
