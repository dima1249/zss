from django.contrib import admin
from django_paranoid.admin import ParanoidAdmin

from lottery.models import LotteryModel, LotteryTickets, LotteryWinner


class TicketInLine(admin.TabularInline):
    model = LotteryTickets
    readonly_fields = ["deleted_at"]
    list_display = ["ticket_number", "user"]
    extra = 1


class WinnerInLine(admin.TabularInline):
    model = LotteryWinner
    readonly_fields = ["deleted_at", "ticket", ]
    list_display = ["user"]
    extra = 1


@admin.register(LotteryModel)
class LotteryModelAdmin(ParanoidAdmin):
    list_display = ['title', 'date', "status"]
    inlines = [WinnerInLine, TicketInLine, ]
