from django.db import models

# Create your models here.
from django_paranoid.models import ParanoidModel


class LotteryStatus(models.IntegerChoices):
    waiting = 1, "Waiting"
    complete = 2, "Complete"
    canceled = 3, "Canceled"


class LotteryModel(ParanoidModel):
    link_to_connect = models.CharField(max_length=256,
                                       unique=True,
                                       blank=True,
                                       null=True,
                                       verbose_name="Холбогдох холбоос")
    title = models.CharField(max_length=256, unique=True, verbose_name="Гарчиг")
    desc = models.TextField(blank=True, null=True, verbose_name="Тайлбар")

    limit_count = models.IntegerField(verbose_name="Оролцох хязгаар")
    winner_count = models.IntegerField(blank=True, null=True, verbose_name="Шагналын тоо")
    ticket_fare = models.IntegerField(blank=True, null=True, verbose_name="Сугалааны өртөг")

    date = models.DateTimeField(verbose_name="Сугалаа тохирол")
    status = models.IntegerField(verbose_name="Төлөв", default=1, choices=LotteryStatus.choices)

    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'lott_lottery'
        verbose_name = 'Сугалаа'
        verbose_name_plural = 'Сугалаанууд'


class LotteryTickets(ParanoidModel):
    lottery = models.ForeignKey("lottery.LotteryModel",
                                on_delete=models.PROTECT,
                                verbose_name="check time",
                                related_name="lot_tickets_set")
    user = models.ForeignKey("account.UserModel",
                             on_delete=models.PROTECT,
                             verbose_name="Оролцогч",
                             related_name="user_tickets_set")
    ticket_number = models.IntegerField(verbose_name="Number", null=True, blank=True)

    ticket_fare = models.IntegerField(verbose_name="Үнэ", null=True, blank=True)

    def __str__(self):
        return '#%s' % self.ticket_number

    def __unicode__(self):
        return self.ticket_number

    class Meta:
        db_table = 'lott_tickets'
        verbose_name = 'Худалдаалагдсан сугалаа'
        verbose_name_plural = 'сугалаанууд'


class LotteryWinner(ParanoidModel):
    lottery = models.ForeignKey("lottery.LotteryModel",
                                on_delete=models.PROTECT,
                                verbose_name="check time",
                                related_name="lot_winners_set")
    ticket = models.ForeignKey("lottery.LotteryTickets",
                               on_delete=models.PROTECT,
                               blank=True,
                               null=True,
                               verbose_name="Сугалаа",
                               related_name="winner_tickets_set")

    winner_price = models.IntegerField(verbose_name="Prize", null=True, blank=True)

    title = models.CharField(max_length=256, unique=True, verbose_name="Гарчиг")
    desc = models.TextField(blank=True, null=True, verbose_name="Тайлбар")

    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'lott_winner'
        verbose_name = 'Хонжвор'
        verbose_name_plural = 'Хонжворууд'
