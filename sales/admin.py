from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
# Register your models here.
from django_paranoid.admin import ParanoidAdmin

from sales.models import *
from zss.custom_tool_classes import DeleteCacheMixin
from zss.utills import generate_qr_token


class VideoTimeModelInline(admin.StackedInline):
    model = VideoTimeModel
    extra = 1
    exclude = ['deleted_at']


@admin.register(VideoModel)
class VideoModelAdmin(ParanoidAdmin):
    list_display = ["name", "loyalty_amount", "video_link", "desc", ]
    inlines = [VideoTimeModelInline]


@admin.register(VideoCategoryModel)
class VideoCategoryModelAdmin(ParanoidAdmin):
    list_display = ["name", "desc", ]


@admin.register(LevelModel)
class LevelAdmin(admin.ModelAdmin):
    list_display = ["id", "level", "user", "invite_code"]


#
#
@admin.register(BannerModel)
class BannerAdmin(SortableAdminMixin, DeleteCacheMixin, ParanoidAdmin):
    CACHE_KEY = StoreItemModel.CACHE_KEY_PREFIX
    list_display = ["title", "my_order", "link", "picture", ]


@admin.register(PointModel)
class PointAdmin(admin.ModelAdmin):
    list_display = ["id", "point", "user"]


@admin.register(GemModel)
class GemAdmin(admin.ModelAdmin):
    list_display = ["id", "quantity", "user"]


class ProductModelInline(admin.StackedInline):
    model = UserProductModel
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    def has_module_permission(self, request):
        return False


@admin.register(ProductModel)
class ProductModelAdmin(ParanoidAdmin):
    list_display = ["title", "bonus", "link", "desc", ]

    inlines = [ProductModelInline]

    def get_readonly_fields(self, request, obj=None):
        return super().get_readonly_fields(request, obj) + ('qr',)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not change or obj.qr is None or len(obj.qr) == 0:
            obj.qr = generate_qr_token(obj.id, prefix="OP")
            obj.save()


@admin.register(StoreItemModel)
class StoreItemModelAdmin(DeleteCacheMixin, ParanoidAdmin):
    CACHE_KEY = StoreItemModel.CACHE_KEY_PREFIX

    list_display = ["title", "picture", "desc", "price"]
