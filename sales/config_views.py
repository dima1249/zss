import requests
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import mixins, viewsets, permissions
from user_agents import parse

from account.models import UserModel
from account.point_models import WatchHistoryModel, TransactionModel
from account.serializers import ProfileMiniSerializer
from sales.models import VideoModel, BannerModel, ProductModel
from sales.serializers import GetPointSerializer, VideoListSerializer, BannerListSerializer, ScanQRSerializer, \
    ProductSerializer, CollectType
from zss.utills import decode_qr_token


class VersionCheck(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    queryset = VideoModel.objects.all()
    permission_classes = [permissions.AllowAny]

    @method_decorator(cache_page(60 ** 2 * 24, key_prefix="config_version"))
    def list(self, request, *args, **kwargs):
        _res = {"success": True,
                "approved_build": 6,
                "data": [],
                }
        return JsonResponse(status=requests.codes.ok, data=_res)


class ClearCache (mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    queryset = VideoModel.objects.all()
    permission_classes = [permissions.AllowAny]

    @method_decorator(cache_page(60 ** 2 * 24, key_prefix="config_version"))
    def list(self, request, *args, **kwargs):
        _res = {"success": True,
                "approved_build": 6,
                "data": [],
                }
        return JsonResponse(status=requests.codes.ok, data=_res)
