# Generated by Django 3.2.12 on 2023-10-06 17:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0009_bannermodel_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videotimemodel',
            name='video',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='video_check_set', to='sales.videomodel', verbose_name='check time'),
        ),
    ]
