# Generated by Django 3.2.12 on 2023-10-26 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0011_productmodel_userproductmodel'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bannermodel',
            options={'ordering': ['my_order'], 'verbose_name': 'Banner', 'verbose_name_plural': 'Banners'},
        ),
        migrations.AddField(
            model_name='bannermodel',
            name='my_order',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
