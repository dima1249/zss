from django.contrib.auth.models import User
from django.db import models
# Create your models here.
from django_paranoid.models import ParanoidModel

from zss.custom_storages import VideoStorage
from zss.utills import PathAndRename


class VideoCategoryModel(ParanoidModel):
    name = models.CharField(max_length=100, unique=True, verbose_name="Нэр")
    desc = models.TextField(verbose_name="Тайлбар", blank=True, null=True)

    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'sales_video_cat'
        verbose_name = 'Үзвэрийн ангилал'
        verbose_name_plural = 'Үзвэрийн ангилалууд'


class VideoModel(ParanoidModel):
    link_to_connect = models.CharField(max_length=256, unique=True, verbose_name="Холбогдох холбоос")
    video_link = models.CharField(max_length=256, unique=True, verbose_name="Үзвэрийн холбоос(Youtube)")
    landscape = models.BooleanField(verbose_name="Video Format Landscape",
                                    default=True)
    img = models.ImageField(verbose_name="Зураг (Landscape)",
                            null=True, blank=True,
                            storage=VideoStorage(),
                            upload_to=PathAndRename("img/"))
    video = models.FileField(verbose_name="Бичлэг (720 < 200mb)",
                             null=True, blank=True,
                             storage=VideoStorage(),
                             upload_to=PathAndRename("video/"))
    # storage = S3Bucket1Storage(),

    loyalty_amount = models.FloatField(verbose_name="Урамшууллын үнэ")
    length = models.IntegerField(blank=True, null=True, verbose_name="Үзвэр урт")
    watch_limit = models.IntegerField(blank=True, null=True, verbose_name="Үзвэр Үзэх хязгаар")
    # users = models.ManyToManyField(User, verbose_name="Үзсэн хэрэглэгчид")
    # time_table = MultiSelectField(choices=TIME_TABLE, verbose_name='Нислэгийн хуваарь', null=True, blank=True)
    desc = models.TextField(blank=True, null=True, verbose_name="Тайлбар")

    host_name = models.CharField(max_length=100, verbose_name="Эзэмшэгчийн нэр", null=True, blank=True)
    name = models.CharField(max_length=100, verbose_name="Content name", default="default")

    category = models.ForeignKey("sales.VideoCategoryModel", on_delete=models.PROTECT, verbose_name="Төрөл", null=True,
                                 related_name="cat_videos")

    def __str__(self):
        return '%s' % self.link_to_connect

    def __unicode__(self):
        return self.link_to_connect

    class Meta:
        db_table = 'sales_video'
        verbose_name = 'Видео Үзвэр'
        verbose_name_plural = 'Видео Үзвэрүүд'


class VideoTimeModel(models.Model):
    video = models.ForeignKey("sales.VideoModel", on_delete=models.PROTECT, verbose_name="check time",
                              related_name="video_check_set")
    duration = models.IntegerField(verbose_name="check minute", null=True, blank=True)

    def __str__(self):
        return '%s' % self.duration

    def __unicode__(self):
        return self.duration

    class Meta:
        db_table = 'sales_video_time'
        verbose_name = 'Шалгах Хугацаа'
        verbose_name_plural = 'Шалгах Хугацаанууд'


class VideoWatchModel(ParanoidModel):
    video = models.ForeignKey("sales.VideoModel", on_delete=models.PROTECT, verbose_name="check time",
                              related_name="watch_video")

    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="check time",
                             related_name="watch_user")

    def __str__(self):
        return '%s %s' % (self.video.id, self.user.id)

    def __unicode__(self):
        return '%s %s' % (self.video.id, self.user.id)

    class Meta:
        db_table = 'sales_video_watch'
        verbose_name = 'Үзвэр ба хэрэглэгч'
        verbose_name_plural = 'Үзвэр ба хэрэглэгчид'


class BannerModel(ParanoidModel):
    CACHE_KEY_PREFIX = 'banner_items'
    picture = models.ImageField(
        verbose_name="Зураг",
        upload_to=PathAndRename("banner/picture/"),
        null=True,
        blank=True,
    )

    title = models.CharField(max_length=100, unique=True, verbose_name="Нэр")
    link = models.CharField(max_length=245, default='', verbose_name="Линк")

    my_order = models.PositiveIntegerField(
        default=0,
        blank=False,
        null=False,
    )

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['my_order']
        db_table = 'promo_banner'
        verbose_name = 'Banner'
        verbose_name_plural = 'Banners'


class PointModel(ParanoidModel):
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="user",
                             related_name="user_points")

    point = models.IntegerField(verbose_name="Point", null=True, blank=True)

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return '%s' % self.id

    class Meta:
        db_table = 'sales_point'
        verbose_name = 'Point'
        verbose_name_plural = 'Points'


class GemModel(ParanoidModel):
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="user",
                             related_name="user_gems")

    quantity = models.IntegerField(verbose_name="quantity", null=True, blank=True)

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return '%s' % self.id

    class Meta:
        db_table = 'sales_gems'
        verbose_name = 'GEM'
        verbose_name_plural = 'GEMs'


class LevelModel(models.Model):
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="user",
                             related_name="user_levels")

    level = models.IntegerField(verbose_name="Level", null=True, blank=True)
    invite_code = models.CharField(max_length=20, verbose_name="invite_code", null=True, blank=True)

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return '%s' % self.id

    class Meta:
        db_table = 'sales_levels'
        verbose_name = 'Level'
        verbose_name_plural = 'Level'


class ProductModel(ParanoidModel):
    title = models.CharField(max_length=100, unique=True, verbose_name="Нэр")
    link = models.CharField(max_length=245, default='', verbose_name="Линк")
    desc = models.TextField(default='', verbose_name="Тайлбар")
    bonus = models.IntegerField(verbose_name="Point", default=0)
    qr = models.CharField(max_length=245, blank=True, null=True, verbose_name="QR")

    user_limit = models.IntegerField(default=-1, verbose_name="Хэдэн хэрэглэгч")
    limit = models.IntegerField(default=-1, verbose_name="Хэдэн удаа")

    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return '%s' % self.title

    class Meta:
        db_table = 'sales_product'
        verbose_name = 'Бараа'
        verbose_name_plural = 'Бараанууд'


class StoreItemModel(ParanoidModel):
    CACHE_KEY_PREFIX = 'store_items'
    title = models.CharField(max_length=100, unique=True, verbose_name="Нэр")
    picture = models.ImageField(
        verbose_name="Зураг",
        upload_to=PathAndRename("store/picture/"),
        null=True,
        blank=True,
    )

    desc = models.TextField(default='', verbose_name="Тайлбар")
    price = models.PositiveIntegerField(
        default=1,
        blank=False,
        null=False,
    )

    price_bonus = models.PositiveIntegerField(
        default=1,
        blank=False,
        null=False,
    )

    def __str__(self):
        return '%s - %s' % (self.title, self.price)

    def __unicode__(self):
        return '%s -  %s' % (self.title, self.price)

    class Meta:
        db_table = 'sales_store_items'
        verbose_name = 'Түц'
        verbose_name_plural = '1. Түц'


class UserProductModel(models.Model):
    product = models.ForeignKey("sales.ProductModel", on_delete=models.PROTECT, verbose_name="Бараа", null=True,
                                related_name="user_product_set")
    user = models.ForeignKey("account.UserModel",
                             on_delete=models.PROTECT,
                             blank=True,
                             null=True,
                             verbose_name="Хэрэглэгч",
                             related_name="product_user_set")

    created_at = models.DateTimeField(auto_now_add=True)
