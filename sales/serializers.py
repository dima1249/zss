from rest_framework import serializers, fields
from django.db import models
from sales.models import VideoModel, BannerModel, ProductModel, StoreItemModel

TRIP_TYPES = [
    ("OW", 'OW'),
    ("RT", 'RT'),
]
CABIN_CODE_TYPES = [
    ('Y', "PremiumEconomy"),
    ('S', "Economy"),
    ('C', "Business"),
    ('J', "PremiumBusiness"),
    ('F', "First"),
    ('P', "PremiumFirst"),
]

COLLECT_TYPES = (
    ('V', 'Video'),
    ('QR', 'QR'),
    ('P', 'Product'),
)


class CollectType(models.TextChoices):
    VIDEO = 'V', 'Video'
    QR = 'QR', 'QR'
    PRODUCT = 'P', 'Product'


class GetPointSerializer(serializers.Serializer):
    key = serializers.CharField(required=True)
    type = serializers.ChoiceField(required=True, choices=CollectType.choices)


class ScanQRSerializer(serializers.Serializer):
    qr = serializers.CharField(required=True)


class VideoListSerializer(serializers.ModelSerializer):
    time_list = serializers.SerializerMethodField()

    def get_time_list(self, obj):
        return obj.video_check_set.values_list('duration', flat=True)

    class Meta:
        model = VideoModel
        fields = [
            "link_to_connect",
            "video_link",

            'img',
            'video',
            'landscape',
            "loyalty_amount",
            "length",
            "watch_limit",
            "desc",
            "host_name",
            "name",
            "category",
            "time_list",
        ]


class BannerListSerializer(serializers.ModelSerializer):
    class Meta:
        model = BannerModel
        fields = ['picture', 'title', 'link']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = [
            "title",
            "link",
            "desc",
            "bonus",
        ]

class StoreItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreItemModel
        fields = "__all__"
