from django.urls import path, include
from rest_framework import routers

from sales.config_views import *

router = routers.DefaultRouter()

# Old Section
router.register(r'version', VersionCheck, 'version')


urlpatterns = [
    path('rest/', include(router.urls)),
    # path('test/', test_view),
]
