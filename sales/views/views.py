import requests
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import mixins, viewsets, permissions
from user_agents import parse

from account.models import UserModel
from account.point_models import WatchHistoryModel, TransactionModel
from account.serializers import ProfileMiniSerializer
from sales.models import VideoModel, BannerModel, ProductModel, StoreItemModel
from sales.serializers import GetPointSerializer, VideoListSerializer, BannerListSerializer, ScanQRSerializer, \
    ProductSerializer, CollectType, StoreItemSerializer
from zss.utills import decode_qr_token


class GetPoint(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    queryset = VideoModel.objects.all()
    serializer_class = GetPointSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():

            _key = serializer.validated_data.get('key')
            # VideoModel.objects.filter(link_to_connect=_key)
            print('_key=', _key)

            if serializer.validated_data.get('type') == CollectType.VIDEO:
                v_model = VideoModel.objects.filter(link_to_connect=_key).first()

                if v_model:
                    wh = WatchHistoryModel.save_history(request.user, v_model)
                    user_agent = parse(request.headers['User-Agent'])
                    TransactionModel.collect(point=v_model.loyalty_amount,
                                             user=request.user,
                                             point_data={
                                                 "key": "WATCH",
                                                 "message": " Video collection",

                                                 "wh": wh.id,
                                                 "v": v_model.id,
                                                 "ug": user_agent.ua_string,
                                             })

                    return JsonResponse(status=requests.codes.ok, data={"success": True, "point": request.user.point})
                return JsonResponse(status=requests.codes.already_reported,
                                    data={"success": False, "message": "No Video"})
            # print('loyalty_amount', str(video.))

            return JsonResponse(status=requests.codes.ok, data={"success": True})
        print(serializer.errors)
        return JsonResponse(status=requests.codes.bad, data={"success": False})
        # return TapaResponse(serializer.errors, status=False, message=gsms.VALIDATION_ERROR, status_code=400)


class VideoList(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.AllowAny]
    queryset = VideoModel.objects.all()
    serializer_class = VideoListSerializer


class BannerList(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.AllowAny]
    queryset = BannerModel.objects.all()
    serializer_class = BannerListSerializer

    @method_decorator(cache_page(60 ** 2 * 24, key_prefix=StoreItemModel.CACHE_KEY_PREFIX))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class StoreList(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    permission_classes = [permissions.AllowAny]
    queryset = StoreItemModel.objects.all()
    serializer_class = StoreItemSerializer

    @method_decorator(cache_page(60 ** 2 * 24, key_prefix=StoreItemModel.CACHE_KEY_PREFIX))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class ScanQR(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
           Description
         """
    queryset = ProductModel.objects.none()
    serializer_class = ScanQRSerializer
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request, **kwargs):
        serializer = self.get_serializer(data=request.GET)
        if serializer.is_valid():
            deencrypt = decode_qr_token(serializer.validated_data['qr'])

            if deencrypt:
                if "USER_ID" in deencrypt:
                    # get_user
                    print(deencrypt['USER_ID'])
                    try:
                        user = UserModel.objects.get(id=deencrypt['USER_ID'])
                        return JsonResponse(status=requests.codes.ok, data={
                            "success": True,
                            "type": 1,
                            "user": ProfileMiniSerializer(instance=user).data
                        })
                    except ObjectDoesNotExist:
                        print('ObjectDoesNotExist')
                    except Exception as e:
                        print('Exception', e)

                elif "PRE" in deencrypt and deencrypt["PRE"] == 'OP':
                    # get product
                    print(deencrypt['ID'])
                    try:
                        product = ProductModel.objects.get(id=deencrypt['ID'])
                        ser_data = ProductSerializer(instance=product).data
                        return JsonResponse(status=requests.codes.ok, data={
                            "success": True,
                            "type": 2,
                            "product": ser_data,
                            "bonus": ser_data.get('bonus')
                        })

                    except ObjectDoesNotExist:
                        print('ObjectDoesNotExist')
                    except Exception as e:
                        print('Exception', e)
                # print(deencrypt)

            return JsonResponse(status=requests.codes.no_content, data={"success": False,
                                                                        "message": "NO DATA"})
        return JsonResponse(status=requests.codes.no_content, data=serializer.errors)
        # return TapaResponse(serializer.errors, status=False, message=gsms.VALIDATION_ERROR, status_code=400)
