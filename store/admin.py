import json
import os

from adminsortable2.admin import SortableAdminMixin
from django.conf.urls import url
from django.contrib import admin, messages
from django.shortcuts import redirect
from django.utils.html import format_html
from django_paranoid.admin import ParanoidAdmin

from account.views.gok import hipay
from store.models import *

our_host_url = os.environ.get("OUR_HOST_URL", "http://127.0.0.1:8083")


@admin.register(PaymentTypeModel)
class PaymentTypeModelAdmin(SortableAdminMixin, ParanoidAdmin):
    search_fields = ["name", "description", "payment_type"]
    list_filter = ["payment_type"]
    list_display = [
        "my_order",
        "name",
        "description",
        "payment_type",
        "fee",
        "is_active",
        "updated_at",
    ]

    def save_model(self, request, obj, form, change):
        obj.updated_by = request.user
        if obj.created_by is None:
            obj.created_by = request.user
        obj.save()

    def has_add_permission(self, request):
        if request.user.is_superuser:
            return True
        else:
            return False

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        else:
            return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(InvoiceModel)
class InvoiceModelAdmin(ParanoidAdmin):
    search_fields = ["ref_number", "checkout_id"]
    list_display = [
        "ref_number",
        "checkout_id",
        "phone",
        "amount",
        "is_paid",
        "updated_at",
        "recheck",
    ]
    list_filter = ["phone"]
    readonly_fields = [
        "ref_number",
        "checkout_id",
        "request_id",
        "expire",
        "amount",
        "is_paid",
        "currency",
        "email",
        "phone",
        "rs_payload",
        "created_at",
        "updated_at",
        "deleted_at",
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def recheck(self, obj):
        if not obj.is_paid:
            _check_button = (
                    f'<a class="button btn-primary" href="{our_host_url}/api/store/v1/check_invoice/'
                    + str(obj.ref_number)
                    + '/HIPAY/">Check</a>&nbsp;'
            )
            return format_html(_check_button, "#")

    def check_balance(self, request):
        rs = hipay.balance()
        if rs and "code" in rs and rs['code'] == 1:
            messages.add_message(
                request,
                messages.SUCCESS,
                f"You have {rs['balance']}",
            )
        else:
            messages.add_message(
                request,
                messages.ERROR,
                f"Error {rs['code']} {json.dumps(rs)}",
            )

        return redirect(
                request.headers.get("referer", "/")
            )


    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                "balance",
                self.check_balance,
                name="hipay_balance",
            ),
        ]
        return custom_urls + urls