import os

import requests
from django.template.loader import get_template


class HiPay:
    def __init__(self, prod=False):
        self.our_server = os.environ.get("OUR_HOST_URL")
        if prod:
            self.server = os.environ.get("PR_HIPAY_URL")
            self.username = os.environ.get("PR_HIPAY_CLIENT_ID")
            self.password = os.environ.get("PR_HIPAY_CLIENT_SECRET")
        else:

            self.server = os.environ.get("HIPAY_URL")
            self.username = os.environ.get("HIPAY_CLIENT_ID")
            self.password = os.environ.get("HIPAY_CLIENT_SECRET")
        self.server_ip = os.environ.get("IP_ADDRESS", "183.177.97.154")

        self.username_wechat = os.environ.get("HIPAY_NAME_WECHAT")
        self.password_wechat = os.environ.get("HIPAY_TOKEN_WECHAT")

    def get_header(self, isWeChat=False):
        return {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + (self.password_wechat if isWeChat else self.password),
        }

    def payment(self, ref_number, checkout_id, amount):
        _url = self.server + "/payment/"
        approval_url = (
                self.our_server
                + "/api/payment/v1/bank_check_invoicev2/"
                + ref_number
                + "/HIPAY/"
        )
        cancel_url = str(os.environ.get("OUR_FRONT_URL")) + "/user/order/"

        _post_data = {
            "checkoutId": checkout_id,
            "amount": amount,
            "shopperResultUrl": approval_url,
            "shopperCancelUrl": cancel_url,
            "lang": "mn",
            "email": "",
            "phone": "",
            "url": _url,
        }
        _body = get_template("hipay_payment_req_body.html").render(_post_data)

        return _body

    # def create_invoice(self, ref_number):
    #     _hipay_invoice_model = HiPayModel.objects.filter(
    #         ref_number=ref_number, is_paid=False
    #     ).last()
    #     if _hipay_invoice_model:
    #         return self.payment(hipay_invoice=_hipay_invoice_model)
    #     else:
    #         return ""

    def create_invoice(self, invoice_number, total_amount, isWeChat=False):

        if isWeChat:
            merchant_name = self.username_wechat
        else:
            merchant_name = self.username

        _post_data = {
            "entityId": merchant_name,
            "amount": total_amount,
            "redirect_uri": "https://zumo.asia/market?id=",
            "currency": "MNT",
            "qrData": True,
            "signal": True,
            "item[0][itemno]": invoice_number,
            "item[0][name]": invoice_number,
            "item[0][price]": total_amount,
            "item[0][quantity]": 1,
        }
        _url = self.server + "/checkout/"

        _response = requests.post(
            url=_url,
            headers=self.get_header(isWeChat),
            json=_post_data,
        )

        error = _response.status_code
        if _response.status_code == requests.codes.ok:
            _response_data = _response.json()
            if _response_data["code"] == 1:
                return _response_data
            print("HIPAY", _response_data)
        return {"ERROR": error, "message": "INVOICE_NOT_CREATED"}

    def check_invoice(self, checkout_id, isWeChat=False):

        _url = (
                self.server + "/checkout/get/" + checkout_id + "/"
        )
        _response = requests.get(
            url=_url, headers=self.get_header(isWeChat=isWeChat)
        )
        if _response.status_code == requests.codes.ok:
            _response_data = _response.json()

            if "status" in _response_data and _response_data["status"] == "paid":
                return {"name": "INVOICE_PAID", "res": _response_data}

            print('hipay invoice not paid : ', _response_data)
            return {
                "code": "INVOICE_NOT_PAID",
                "message": "INVOICE_NOT_PAID",
            }
        return {"code": "INVOICE_NOT_PAID", "message": "INVOICE_NOT_PAID"}

    def add_card(self, customer_id):

        _params = {
            "entityId": self.username,
            "customer_id": customer_id,
            "access_token": self.password,
            "redirect_uri": self.our_server + "/api/payment/v1/bank_cancel_invoice/",
        }
        header = {"Content-Type": "application/json"}

        _url = (
                self.server
                + "/v2/card/add/"
                + "?entityId="
                + self.username
                + "&customer_id="
                + customer_id
                + "&access_token="
                + self.password
                + "&redirect_uri="
                + self.our_server
                + "/api/payment/v1/bank_cancel_invoice/"
        )
        # _response = requests.get(url=_url, headers=header, params=_params)
        # if _response.status_code == requests.codes.ok:
        #     #_response_data = _response.json()
        #     tapa_print(_response.text)
        #     return _response
        return _url

    # MTP

    def balance(self):

        _url = (
                self.server + "/txn/m2p/balance"
        )
        _response = requests.post(
            url=_url,
            headers=self.get_header(),
            json={"entityId": self.username},
        )

        error = _response.status_code
        if _response.status_code == requests.codes.ok:
            _response_data = _response.json()
            # if _response_data["code"] == 1:
            #     return _response_data
            print("HIPAY balance", _response_data)
            return _response_data
        print(_response.status_code, _response.json())

        return {"code": _response.status_code, "message": _response.json().get('message', 'no message')}

    def withdraw(self, customer_id, amount, desc, t_id):

        _url = (
                self.server + "/txn/m2p"
        )
        _post_data = {
            "entityId": self.username,
            "customerId": customer_id,
            "amount": amount,
            "txndesc": desc,
            "ipaddress": self.server_ip,
            "trackId": t_id
        }

        print('withdraw rq: ', _post_data)
        _response = requests.post(
            url=_url,
            headers=self.get_header(),
            json=_post_data,
        )

        error = _response.status_code
        if error == requests.codes.ok:
            _response_data = _response.json()
            # if _response_data["code"] == 1:
            #     return _response_data
            print("HIPAY withdraw", _response_data)
            return _response_data
        print(_response.status_code, _response.json())

        return {"code": _response.status_code, "message": _response.json().get('message', 'no message')}

    # {{UrlCHP}}/txn/m2p/balance
    # def get_card_info(self, card_id):
    #     _url = self.server + "/v2/card/add/" + card_id
    #     _response = requests.get(url=_url, headers=self.get_header())
    #     if _response.status_code == requests.codes.ok:
    #         _response_data = _response.json()
    #         return _response_data
