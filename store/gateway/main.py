import os

from store.gateway.hipay import HiPay
from store.models import InvoiceModel

hipay = HiPay()


def generateRefNumber(invoice_data=None):
    return f"SI{invoice_data.get('item_id', 1)}{invoice_data.get('payment', 'NN')}{invoice_data.get('user_id', 1)}"


class PaymentGateWay:
    def __init__(self):
        self.our_server = os.environ.get("OUR_HOST_URL", "http://127.0.0.1:8083/")

    def check_invoice(self, ref_number, payment_type='HIPAY'):
        hipay_invoice_model = InvoiceModel.objects.filter(ref_number=ref_number).order_by("created_at").last()
        if hipay_invoice_model:
            if hipay_invoice_model.is_paid:
                return True

            # check payment_type
            if payment_type == "HIPAY":
                hipay_res = hipay.check_invoice(
                    checkout_id=hipay_invoice_model.checkout_id
                )

                if "code" in hipay_res:
                    if hipay_res['code'] == 'INVOICE_PAID':
                        hipay_invoice_model.is_paid = True
                        hipay_invoice_model.rs_payload = hipay_res['res']
                        hipay_invoice_model.save()

    def create_invoice(self, invoice_data):
        payment = invoice_data.get("payment")
        ref_number = generateRefNumber(invoice_data=invoice_data)
        total_amount = invoice_data['amount']
        # total_amount = calculate_total_amount(
        #     booking_model=booking_model,
        #     fee=payment_type_model.fee,
        # )
        # _now = datetime.now() - timedelta(minutes=10)
        # if HiPayModel.objects.filter(
        #     ref_number=booking_model.ref_number,
        #     amount=total_amount,
        #     is_paid=False,
        #     phone=payment_type_model.name,
        #     created_at__gt=_now,
        # ).exists():
        #     _hipay_invoice_model = HiPayModel.objects.filter(
        #         ref_number=booking_model.ref_number,
        #         amount=total_amount,
        #         is_paid=False,
        #         phone=payment_type_model.name,
        #     ).last()

        invoice_number = ref_number

        invoice_model = InvoiceModel.objects.filter(
            ref_number=ref_number, is_paid=False, source=invoice_data.get('payment_name')
        ).last()

        # check the invoice has expired
        # if expired create new one

        if not invoice_model:

            # return hipay.payment(hipay_invoice=_hipay_invoice_model)

            # if no model
            # if hipay
            _response_data = hipay.create_invoice(invoice_number=invoice_number, total_amount=total_amount)
            if not "ERROR" in _response_data:
                invoice_model = InvoiceModel()
                invoice_model.checkout_id = _response_data["checkoutId"]
                invoice_model.request_id = _response_data["requestId"]
                invoice_model.expire = _response_data["expires"]
                invoice_model.ref_number = ref_number
                invoice_model.amount = total_amount
                invoice_model.phone = payment
                invoice_model.rs_payload = _response_data
                invoice_model.save()
            else:
                print('hipay.create_invoice', _response_data['ERROR'])
                return






        # _redirect_url = (
        #         self.our_server
        #         + "/api/payment/v1/redirect/?ref_number="
        #         + str(ref_number)
        #         + "&payment_type=HIPAY"
        # )

        _redirect_url = (
                self.our_server
                + "/api/store/v1/check_invoice/"
                + str(ref_number)
                + "/HIPAY"
        )

        _result = {
            "url": _redirect_url,
            "checkout_id": invoice_model.checkout_id,
            "deeplink": "hipaywallet://checkout/"
                        + invoice_model.checkout_id
                        + "/https://zumo.asia/market?id=",
            "qr_text": invoice_model.rs_payload["qrData"]
            if "qrData" in invoice_model.rs_payload
            else "EROOR",
        }
        return _result

    def web_hook(self, checkout_id):

        hipay_invoice_model = InvoiceModel.objects.filter(checkout_id=checkout_id).last()
        if hipay_invoice_model:
            if hipay_invoice_model.is_paid:
                return True

            hipay_res = hipay.check_invoice(
                checkout_id=checkout_id
            )

            if "code" in hipay_res:
                if hipay_res['code'] == 'INVOICE_NOT_PAID':
                    hipay_invoice_model.is_paid = True
                    hipay_invoice_model.rs_payload = hipay_res['res']
                    hipay_invoice_model.save()
                #
                # amount = calculate_paid_actual_amount(
                #     amount=_hipay_invoice_model.amount,
                #     fee=payment_type_model.fee
                #     if payment_type_model.fee and payment_type_model.fee > 0
                #     else 0,
                # )
                # _transaction = save_tapa_transaction(
                #     ref_number=_hipay_invoice_model.ref_number,
                #     payment_type=payment_type_model.name,
                #     payment_id=_hipay_invoice_model.checkout_id,
                #     payment_description=_hipay_invoice_model.ref_number,
                #     received_amount=amount,
                # )
                # charge_payment(
                #     ref_number=_transaction.ref_number, amount=_transaction.amount
                # )
                # _transaction.charge_payment_called = (
                #         _transaction.charge_payment_called + 1
                # )
                # _transaction.save()

                if hipay_res['code'] == 'INVOICE_NOT_PAID':
                    hipay_invoice_model.expire = "0"
                    hipay_invoice_model.save()

        return True
