# Generated by Django 3.2.12 on 2023-07-27 21:23

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SellItemModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(blank=True, default=None, null=True)),
                ('quantity', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0)], verbose_name='quantity')),
                ('type', models.IntegerField(choices=[(0, 'Point'), (1, 'Level'), (2, 'Gem')], default=0, verbose_name='Төрөл')),
                ('price', models.FloatField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0.0)], verbose_name='price')),
            ],
            options={
                'verbose_name': 'Store Item',
                'verbose_name_plural': 'Store Items',
                'db_table': 'store_item',
            },
        ),
        migrations.CreateModel(
            name='SellTransaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(blank=True, default=None, null=True)),
                ('item', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='sell_items', to='store.sellitemmodel', verbose_name='Бараа')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='user_sells', to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'Sell Transaction',
                'verbose_name_plural': 'Sell Transactions',
                'db_table': 'store_transaction',
            },
        ),
    ]
