from django.core.validators import MinValueValidator
from django.db import models

from django_paranoid.models import ParanoidModel


class SellItemModel(ParanoidModel):
    class SellType(models.IntegerChoices):
        POINT = 0, "Point"
        LEVEL = 1, "Level"
        GEM = 2, "Gem"

    quantity = models.IntegerField(verbose_name="quantity", null=True, blank=True, validators=[MinValueValidator(0)])
    type = models.IntegerField(verbose_name="Төрөл",
                               default=SellType.POINT,
                               choices=SellType.choices, )
    price = models.FloatField(verbose_name="price", null=True, blank=True, validators=[MinValueValidator(0.0)])

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return '%s' % self.id

    class Meta:
        db_table = 'store_item'
        verbose_name = 'Store Item'
        verbose_name_plural = 'Store Items'


class SellTransaction(ParanoidModel):
    item = models.ForeignKey("store.SellItemModel", on_delete=models.PROTECT, verbose_name="Бараа", null=True,
                             related_name="sell_items")
    user = models.ForeignKey("account.UserModel", on_delete=models.PROTECT, verbose_name="user",
                             related_name="user_sells")

    def __str__(self):
        return '%s' % self.id

    def __unicode__(self):
        return '%s' % self.id

    class Meta:
        db_table = 'store_transaction'
        verbose_name = 'Sell Transaction'
        verbose_name_plural = 'Sell Transactions'

