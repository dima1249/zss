from django.core.validators import MinValueValidator
from django.db import models

from django_paranoid.models import ParanoidModel

from zss.utills import PathAndRename

max_length_id = 50
min_length_id = 20
max_length_name = 150

TRANSACTION_TYPE = [
    ("IN", "ОРЛОГО"),
    ("EX", "ЗАРЛАГА"),
]

PAYMENT_TYPE = [
    ("Ecommerce", "Ecommerce"),
    ("Corporate gateway", "Corporate gateway"),
    ("Payment app", "Payment app"),
]


class PaymentSource(models.TextChoices):
    ZUMO = 'ZUMO', 'Zumo point'
    HIPAY = 'HIPAY', 'Hipay app'
    MONPAY = 'MONPAY', 'Monpay app'


class PaymentTypeModel(ParanoidModel):
    name = models.CharField(
        max_length=max_length_name,
        null=True,
        blank=True,
        verbose_name="Тооцооны хэрэгсэлийн нэр",
    )

    # payment_group = models.ForeignKey(
    #     "gok.PaymentGroupModel",
    #     on_delete=models.PROTECT,
    #     null=True,
    #     verbose_name="Төлбөрийн бүлэг",
    #     related_name="payment_groups",
    # )

    description = models.CharField(
        max_length=max_length_name,
        null=True,
        blank=True,
        verbose_name="Тооцооны хэрэгсэлийн тайлбар нэр",
    )
    account_number = models.CharField(
        max_length=max_length_id,
        null=True,
        blank=True,
        verbose_name="Хүлээн авах дансны дугаар",
    )
    company_name = models.CharField(
        max_length=max_length_name,
        default="Тапатрип Эжэнси ХХК",
        null=True,
        blank=True,
        verbose_name="Хүлээн авагчийн нэр",
    )
    payment_type = models.CharField(
        max_length=max_length_id,
        choices=PAYMENT_TYPE,
        null=True,
        blank=True,
        verbose_name="Гүйлгээний хэлбэр",
    )

    transaction_type = models.IntegerField(
        choices=TRANSACTION_TYPE, null=True, blank=True, verbose_name="Гүйлгээний төрөл"
    )
    fee = models.FloatField(null=True, blank=True, verbose_name="Шимтгэлийн хувь")

    currency = models.CharField(
        default="MNT",
        max_length=10,
        null=True,
        blank=True,
        verbose_name="Мөнгөн тэмдэгт",
    )
    logo = models.ImageField(
        verbose_name="Лого",
        upload_to=PathAndRename("paymentlogos/"),
        null=True,
        blank=True,
    )

    is_active = models.BooleanField(
        default=True,
        verbose_name="Төлбөрийн хэрсэлээр ашиглаж болох",
        null=True,
        blank=True,
    )
    my_order = models.PositiveIntegerField(
        default=0, blank=False, null=False, verbose_name="эрэмбэ"
    )

    created_by = models.ForeignKey(
        "account.UserModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name="Үүсгэсэн хэрэглэгч",
        related_name="created_payment_type_user",
    )
    updated_by = models.ForeignKey(
        "account.UserModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name="Шинэчилсэн хэрэглэгч",
        related_name="updated_payment_type_user",
    )
    deleted_by = models.ForeignKey(
        "account.UserModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name="Устгасан хэрэглэгч",
        related_name="deleted_payment_types_user",
    )

    def __str__(self):
        return "%s" % self.name

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["my_order"]
        db_table = "store_payment_types"
        verbose_name = "Payment Type"
        verbose_name_plural = "Payment Types"


class InvoiceModel(ParanoidModel):
    ref_number = models.CharField(
        max_length=max_length_id, null=True, blank=True, verbose_name="ref number"
    )

    version = models.IntegerField(
        verbose_name="Version",
        default=1
    )

    source = models.CharField(
        max_length=max_length_id,
        choices=PaymentSource.choices,
        verbose_name="Тооцооны хэрэглүүр",
    )


    checkout_id = models.CharField(
        max_length=max_length_id, null=True, blank=True, verbose_name="Шалгах id"
    )
    request_id = models.CharField(
        max_length=max_length_id, null=True, blank=True, verbose_name="Хүсэлтийн id"
    )
    expire = models.CharField(
        max_length=min_length_id, null=True, blank=True, verbose_name="дуусах хугацаа"
    )
    amount = models.FloatField(
        default=0, null=True, blank=True, verbose_name="Гүйлгээний дүн"
    )
    is_paid = models.BooleanField(
        default=False, null=True, blank=True, verbose_name="Төлсөн"
    )
    currency = models.CharField(
        default="MNT",
        max_length=10,
        null=True,
        blank=True,
        verbose_name="Мөнгөн тэмдэгт",
    )
    email = models.EmailField(null=True, blank=True, verbose_name="email")
    phone = models.CharField(
        max_length=min_length_id, null=True, blank=True, verbose_name="утасны дугаар"
    )
    rs_payload = models.JSONField(blank=True, null=True, verbose_name="Response")

    def __str__(self):
        return "%s" % self.ref_number

    def __unicode__(self):
        return self.ref_number

    class Meta:
        db_table = "store_invoices"
        verbose_name = "Invoice"
        verbose_name_plural = "Invoices"
