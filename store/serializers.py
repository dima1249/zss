from rest_framework import serializers


class InvoiceSerializer(serializers.Serializer):

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    item_id = serializers.IntegerField(required=True)
    ref_number = serializers.CharField(required=False, max_length=100)
    payment = serializers.CharField(required=True, max_length=200)
    now_pay_amount = serializers.FloatField(required=False, default=0)

