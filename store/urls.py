from django.urls import path, include
from rest_framework import routers

from store.views import InvoiceCreateView, HiPayCheckInvoice, CheckInvoiceV2

router = routers.DefaultRouter()

# restRouter.register(r"payment_groups", PaymentGroups, "PaymentGroups")
router.register(r"invoice", InvoiceCreateView, "Invoice")
# router.register(r"redirect", RedirectInvoice, "RedirectInvoice")
router.register(
    r"check_invoice/(?P<ref_number>[\w]+)/(?P<payment_type>[\w]+)",
    CheckInvoiceV2,
    "CheckInvoiceV2",
)
# restRouter.register(
#     r"bank_check_invoicev2/(?P<ref_number>[FSRNLGUWQJ]{1,2}\d+[K]\d+|[FSRNLGUWQJ]{1,2}\d+)/(?P<payment_type>[\w]+)",
#     BankCheckInvoiceV2,
#     "BankCheckInvoiceV2",
# )

# restRouter.register(r"bank_cancel_invoice", BankCancelInvoice, "BankCancelInvoice")


router.register(r"bank_check_hipay", HiPayCheckInvoice, "HiPayCheckInvoice")

urlpatterns = [
    path("v1/", include(router.urls)),
    # path(
    #     "manual_charge_form/<str:transaction_id>/",
    #     AllTransactionManualChargeView.as_view(),
    #     name="gok-manual-charge-form",
    # ),
]