import _thread

from rest_framework import mixins, viewsets, serializers
from rest_framework import permissions

from sales.models import StoreItemModel
from store.gateway.main import PaymentGateWay
from store.models import PaymentTypeModel
from store.serializers import InvoiceSerializer
from zss.custom_response_utils import CustomResponse

payment_gateway = PaymentGateWay()


class InvoiceCreateView(
    mixins.CreateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = InvoiceSerializer

    # throttle_classes = [CustomUserRateThrottle]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        _result = {"name": "INVALID", "message": "VALIDATION_ERROR"}
        if serializer.is_valid():

            payment_name = serializer.validated_data.get("payment")
            item_id = serializer.validated_data.get("item_id")
            try:
                invoice_data = serializer.validated_data
                item = StoreItemModel.objects.get(id=item_id)
                # check MNT source 
                invoice_data['amount'] = item.price
                invoice_data['user_id'] = request.user.id

                if payment_name == "POINT":
                    pass

                    # _result = tapapay.create_invoice(
                    #     _booking_model, payment_type, request.user
                    # )
                    # if "name" in _result:
                    #     return TapaResponse(
                    #         result=_result,
                    #         status=False,
                    #         message=gsms.INVOICE_NOT_CREATED,
                    #         status_code=208,
                    #     )
                    # invoice_info = {}
                    # invoice_info["is_token"] = False
                    # invoice_info["token"] = ""
                    # invoice_info["is_url"] = False
                    # invoice_info["url"] = ""
                    # invoice_info["is_qr"] = True
                    # invoice_info["qr_text"] = _result["qr_code"]
                    # invoice_info["is_deeplink"] = False
                    # invoice_info["tapapay"] = _result
                    # invoice_info["deeplink"] = []
                    #
                    # return TapaResponse(
                    #     result=invoice_info, status=True, message=gsms.SUCCESS
                    # )

                _result = payment_gateway.create_invoice(invoice_data=invoice_data)

                payment_type = PaymentTypeModel.objects.filter(
                    name=payment_name
                ).first()

                hipay_deeplink = {}
                hipay_deeplink["name"] = payment_name
                hipay_deeplink["logo_from_our_system"] = True
                hipay_deeplink["logo"] = str(payment_type.logo)
                hipay_deeplink["deeplink"] = _result["deeplink"]
                hipay_deeplink["fee"] = payment_type.fee

                invoice_info = {}
                invoice_info["is_token"] = False
                invoice_info["token"] = ""
                invoice_info["is_url"] = True
                invoice_info["url"] = _result["url"]
                invoice_info["checkout_id"] = _result["checkout_id"]
                invoice_info["is_qr"] = False
                invoice_info["qr_text"] = ""
                invoice_info["is_deeplink"] = True
                invoice_info["deeplink"] = [hipay_deeplink]

                return CustomResponse(
                    result=invoice_info, status=True, message='SUCCESS'
                )





            except Exception as e:
                print("Exception:", e)
                raise e
                _result = {"name": "ERROR", "message": "PAYMENT_ORDER_NOT_FOUND"}

        return CustomResponse(
            result=_result,
            status=False,
            message=_result['message'],
            status_code=208
        )


class BankCheckHipaySerializer(serializers.Serializer):
    checkoutId = serializers.CharField(required=False, help_text="checkoutId")


class HiPayCheckInvoice(
    mixins.ListModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet
):
    permission_classes = [permissions.AllowAny]
    serializer_class = BankCheckHipaySerializer

    def list(self, request, *args, **kwargs):

        try:
            checkout_id = request.query_params.get("checkoutId")
            if checkout_id:
                _thread.start_new_thread(payment_gateway.web_hook, (checkout_id,))
            return CustomResponse(
                result="",
                status=True,
                message='SUCCESS',
                status_code=200,
            )
        except Exception as e:
            print("HiPayCheckInvoice Exception", e)
            return CustomResponse(
                result="",
                status=True,
                message="INVOICE_NOT_FOUND",
                status_code=404,
            )

    def create(self, request, *args, **kwargs):

        try:
            checkout_id = request.query_params.get("checkoutId")
            if checkout_id:
                _thread.start_new_thread(payment_gateway.web_hook, (checkout_id,))
            return CustomResponse(
                result="",
                status=True,
                message='SUCCESS',
                status_code=200,
            )
        except Exception as e:
            print("hipay webhook", e)
            return CustomResponse(
                result="",
                status=True,
                message='INVOICE_NOT_FOUND',
                status_code=404,
            )


#
class CheckInvoiceV2(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = [permissions.AllowAny]

    def list(self, request, *args, **kwargs):

        if "ref_number" in kwargs and "payment_type" in kwargs:

            _ref_number_temp = kwargs["ref_number"]
            _ref_number = _ref_number_temp.split("K")[0]
            payment_type_name = kwargs["payment_type"]

            _response_data = payment_gateway.check_invoice(ref_number=_ref_number,
                                                           payment_type=payment_type_name)
            # _booking_model = BookingProvider.get_booking_model_data(
            if _response_data and _response_data["name"] == "INVOICE_PAID":
                return CustomResponse(
                    result=_response_data,
                    status=True,
                    message='PAYMENT_SUCCESS',
                    status_code=200,
                )
            elif _response_data and _response_data["name"] == "INVOICE_ALREADY_PAID":
                return CustomResponse(
                    result=_response_data,
                    status=True,
                    message='PAYMENT_SUCCESS',
                    status_code=200,
                )
            else:

                return CustomResponse(
                    result=_response_data,
                    status=False,
                    message='PAYMENT_UNSUCCESS',
                    status_code=208,
                )

        return CustomResponse(
            result=None,
            status=False,
            message='PAYMENT_NOT_FOUND',
            status_code=208,
        )
