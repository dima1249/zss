from rest_framework_simplejwt.authentication import *

class ZumoJWTAuthentication(JWTAuthentication):
    def get_user(self, validated_token):
        _fields = [
            "id", 
            "phone",
            "username",
            "is_active",
            "role"
            ]
        try:
            user_id = validated_token[api_settings.USER_ID_CLAIM]
        except KeyError:
            raise InvalidToken("TOKEN_VALIDATOR_ERROR")

        try:
            user = self.user_model.objects.only(*_fields).get(**{api_settings.USER_ID_FIELD: user_id})
        except self.user_model.DoesNotExist:
            raise AuthenticationFailed("ACCOUNT_USER_FIND_ERROR", code="user_not_found")
        
        if not user.is_active:
            raise AuthenticationFailed("ACCOUNT_CANNOT_LOGIN", code="user_inactive")

        return user
