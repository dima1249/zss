# custom_storages.py file content, file should be placed under top-level project directory
import os

from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage



class StaticStorage(S3Boto3Storage):
    location = settings.STATICFILES_LOCATION


class MediaStorage(S3Boto3Storage):
    location = settings.MEDIAFILES_LOCATION


AWS_VIDEO_STORAGE_BUCKET_NAME = os.environ.get("AWS_VIDEO_STORAGE_BUCKET_NAME")


class VideoStorage(S3Boto3Storage):
    # location = ''
    # file_overwrite = False
    bucket_name = AWS_VIDEO_STORAGE_BUCKET_NAME
    custom_domain = "%s.s3.%s.amazonaws.com" % (
        AWS_VIDEO_STORAGE_BUCKET_NAME,
        settings.AWS_S3_REGION_NAME,
    )
