from django.core.cache import cache
from django.contrib import admin

def flush_cache(key_list=None):
    if key_list is None:
        key_list = []
    if not isinstance(key_list, list):
        key_list = [key_list]
    # cache.keys('*')

    cnt = 0
    for text in key_list:
        for key in list(cache.keys("*")):
            if text in key:
                cnt = cnt + 1
                print("flush_cache key - ", key)
                cache.delete(key.replace(":1:", ""))

    return cnt


class DeleteCacheMixin(admin.ModelAdmin):
    def response_action(self, request, queryset):

        if hasattr(self, "CACHE_KEY"):
            flush_cache(self.CACHE_KEY)

        return super().response_action(request, queryset)

    def delete_model(self, request, obj):
        super().delete_model(request, obj)

        if hasattr(self, "CACHE_KEY"):
            flush_cache(self.CACHE_KEY)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        if hasattr(self, "CACHE_KEY"):
            flush_cache(self.CACHE_KEY)
